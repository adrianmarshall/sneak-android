package com.sneak8.sneak8;

import com.sneak8.utility.common;

import org.junit.Test;

import static com.sneak8.utility.common.getAllLiveStreams;
import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void getAllStreams() throws Exception {
        String availableStreams = common.getAllLiveStreams();
        System.out.println("Available streams: " + availableStreams);
        if(availableStreams.isEmpty()){
            availableStreams = null;
        }
        assertNotNull(availableStreams);
    }
}