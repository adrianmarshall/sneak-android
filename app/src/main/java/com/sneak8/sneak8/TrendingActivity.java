package com.sneak8.sneak8;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.red5pro.streaming.R5Stream;
import com.red5pro.streaming.config.R5Configuration;
import com.sneak8.sneak8.fragments.LocationDialog;
import com.sneak8.sneak8.fragments.LocationDialog.LocationDialogListener;
import com.sneak8.utility.AWSHelper;
import com.sneak8.utility.SimpleLocationManager;
import com.squareup.picasso.Picasso;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Universal Image loader imports



/**
 * Created by adrian on 11/3/15.
 */
public class TrendingActivity extends FragmentActivity implements LocationDialogListener, OnClickListener {


    //    Resources res = getResources();

    // fields for an event that we will retreive from the api call
    static final String TAG_ID = "id";
    static final String TAG_BUSSINESS_ID = "business";      // ID number that correlates to the business hosting the event. Use this to retrieve business object
    static final String TAG_TITLE = "title";
    static final String TAG_DATE = "event_date";
    static final String TAG_START_TIME = "startTime";
    static final String TAG_END_TIME = "endTime";
    static final String TAG_DESCRIPTION = "description";
    static final String TAG_VIEWS = "views";
    static final String TAG_PRICE = "price";
    static final String TAG_EVENT_IMAGE = "photo";
    static String BASE_URL = "http://10.0.0.150:8000";      // Home-> "http://10.0.0.11:8000";      // Cafe Intermezzo- "http://1.15.254.158:8000         // starbucks-norcross "http://172.31.98.51:8000";      //home "http://10.0.0.11:8000";      // normal local host address. 172.31... is WiFi connected IP.. http://10.0.0.12:8000";

    //static final String BASE_URL = "http://10.0.0.5:8000/";
    ArrayList<HashMap<String,String>> eventList;        // List to hold events


    ImageLoader mImageLoader = ImageLoader.getInstance();
    ImageView videoView;
    TextView txt_views;       // View count for event
    TextView txt_title;       // Title for the event that shows
    ImageView btnProfile;
    FloatingActionButton fabPublish;
    SimpleLocationManager locationManager;      // Location manager to get users location

    AWSHelper awsHelper;
    // Amazon TransferObserver for transfering files
    TransferObserver observer;

    // The SimpleAdapter adapts the data about transfers to rows in the UI
    private SimpleAdapter simpleAdapter;

    // This is the main class for interacting with the Transfer Manager
    private TransferUtility transferUtility;



    // A List of all transfers
    private List<TransferObserver> observers = new ArrayList<TransferObserver>();

    /**
     * This map is used to provide data to the SimpleAdapter above. See the
     * fillMap() function for how it relates observers to rows in the displayed
     * activity.
     */
    private ArrayList<HashMap<String, Object>> transferRecordMaps;

    // Bi-Directional map from Google
    BidiMap<String,String> eventsToFilePathMap = new DualHashBidiMap();            // This map will correlate event IDs to their associated files.

    //R5Pro Stream config
    R5Stream stream;
    boolean isSubscribing = false;

    R5Configuration configuration;

//    ProgressDialog progress = new ProgressDialog(this);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending2);

       //  BASE_URL = res.getString(R.string.domain);     // sets the URL/home page/base domain

        videoView = (ImageView) findViewById(R.id.focusedEvent);        // get temporary event image placeholder
        txt_views = (TextView) findViewById(R.id.txt_viewCount);
        txt_title = (TextView) findViewById(R.id.txt_title);
        btnProfile = (ImageView) findViewById(R.id.btnProfile);
        fabPublish = (FloatingActionButton) findViewById(R.id.fab_publish);

        btnProfile.setOnClickListener(this);
        fabPublish.setOnClickListener(this);

        //ImageView tempBackground = (ImageView) findViewById(R.id.imgTrendingTemp);
        eventList = new ArrayList<HashMap<String,String>>();    // initiate eventList HashMap for listview of events

        // Create global configuration and initialize ImageLoader with this config
        initImageLoader(getApplicationContext());

        // Initialize AWS Helper object to initialize AWS Credentials
        awsHelper  = new AWSHelper(getApplicationContext() );
        transferRecordMaps = new ArrayList<HashMap<String, Object>>();


        new getEvents("adrian","denver10").execute();       // TODO retrieve username and password first from storage, then pass as arguments

        // Show Dialog box to set users location
      //  showLocationDialog();
       // locationManager = new SimpleLocationManager(getApplicationContext());
        //locationManager.startLocation();

        //get listview
        ListView lv;

        // TODO come back and change this divider later to a more reasonable and appealing color/image
      //  lv.setDivider(this.getResources().getDrawable(R.drawable.sneak8_splash));		// Setting a divider between the list elements
        //lv.setDividerHeight(15);

    }


    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
    }
    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").

        // Clear transfer listeners to prevent memory leak, or
        // else this activity won't be garbage collected.
        if (observers != null && !observers.isEmpty()) {
            for (TransferObserver observer : observers) {
                observer.cleanTransferListener();
            }
        }


    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }

    @Override
    public void onClick(View v) {

        if(v == fabPublish){
            Intent streamingActivity = new Intent(getApplicationContext(),StreamingActivity.class);
            startActivity(streamingActivity);
        }

        if(v == btnProfile){
            // Gets the last location and show's it (currently in a TOAST format)
            locationManager.showLocation(locationManager.getLocation());
        }

    }


    /**
     * Gets all relevant transfers from the Transfer Service for populating the
     * UI
     */
    private void initData() {
        transferRecordMaps.clear();
        // Uses TransferUtility to get all previous download records.
        observers = transferUtility.getTransfersWithType(TransferType.DOWNLOAD);
        TransferListener listener = new MyDownloadListener();
        for (TransferObserver observer : observers) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            awsHelper.fillMap(map, observer);
            transferRecordMaps.add(map);

            // Sets listeners to in progress transfers
            if (TransferState.WAITING.equals(observer.getState())
                    || TransferState.WAITING_FOR_NETWORK.equals(observer.getState())
                    || TransferState.IN_PROGRESS.equals(observer.getState())) {
                observer.setTransferListener(listener);
            }
        }
        simpleAdapter.notifyDataSetChanged();
    }


    public class getEvents extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        // TODO Store user credentials at Login, Retrieve user credentials here and use.
        getEvents(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.


            try {
                String addr = BASE_URL +"/events/~api/v1/?format=json";

                URL url = new URL(addr);
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                httpCon.setUseCaches(false);
                httpCon.setAllowUserInteraction(false);
                String userPassword = mEmail + ":" + mPassword;
                String encoding = Base64.encodeToString(userPassword.getBytes(), Base64.DEFAULT);

                httpCon.addRequestProperty("Authorization", "Basic " + encoding);

                System.out.println(httpCon.getResponseCode());

                // Get the data returned
                StringBuilder results = new StringBuilder();
                InputStream in = new BufferedInputStream(httpCon.getInputStream());     // Get input stream

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));  // read in the data from input stream

                String line = "";

                // loop through the data and build the string of results from the http request.
                while((line = reader.readLine()) != null){
                    results.append(line);
                }

                System.out.println(results.toString());     // for dev/test purposes, output the results

                JSONObject allData = new JSONObject(results.toString());
                Log.d("Number of events: ", allData.getString("count"));
                JSONArray eventResults = allData.getJSONArray("results");    // Gets all of the results/ events

                System.out.println("JSON 1st object: " + eventResults.getJSONObject(0));
                //System.out.println("40 Watts event id should be 4-> : " + jsonResults.getString("business"));

                // TODO finish implementing. Get data from 'event json array'

                if(eventResults != null) {
                    //Looping through all allData
                    for (int i = 0; i < eventResults.length(); i++) {

                        JSONObject e = eventResults.getJSONObject(i);


                        // Storing each json item values in variable
                        String id = e.getString(TAG_ID);
                        String business_id = e.getString(TAG_BUSSINESS_ID);
                        String title = e.getString(TAG_TITLE);
                        String event_date = e.getString(TAG_DATE);
                        String startTime = e.getString(TAG_START_TIME);
                        String endTime = e.getString(TAG_END_TIME);

                        String description = e.getString(TAG_DESCRIPTION);
                        String views = e.getString(TAG_VIEWS);
                        String price = e.getString(TAG_PRICE);
                        String event_image = e.getString(TAG_EVENT_IMAGE);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key > value
                        map.put(TAG_ID, id);
                        map.put(TAG_TITLE, title);
                        // map.put(TAG_LIKES, likes); 	** NOT YET IMPLEMENTED ON SERVER SIDE
                        map.put(TAG_EVENT_IMAGE, event_image);
                        map.put(TAG_START_TIME, startTime);
                        map.put(TAG_DESCRIPTION, description);
                        map.put(TAG_VIEWS,views);


                        //adding HashList to ArrayList
                        eventList.add(map);


                    }

                    Log.d("eventList: ",eventList.toString() );
                    // adds list of events to a list view
                    // TODO change this to add events to a carousel
                 //   mAdapter = new EventListAdapter(TrendingActivity.this, eventList);
                   //  getListView().setAdapter(mAdapter);

                }
                httpCon.disconnect();
            } catch(Exception e){
                Log.d("error", e.getMessage());
            }


            return true;
        }

        // TODO: register the new account here.
        // return true;


        // @Override
        protected void onPostExecute(final Boolean success) {


            if (success) {
                Toast.makeText(TrendingActivity.this, "Here's what's going on", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(TrendingActivity.this,"Error occured", Toast.LENGTH_SHORT);
            }


            for(HashMap<String,String> event : eventList){

                observer = awsHelper.downloadS3File(event.get(TAG_EVENT_IMAGE));
                event.put("fileName",observer.getAbsoluteFilePath());       // store event filePath for assocated image/video
                Log.d("fileNameToEvent: ", "Event ID: " +event.get(TAG_ID) + ", File Path: " + observer.getAbsoluteFilePath());

                observer.setTransferListener(new MyDownloadListener());
                eventsToFilePathMap.put(event.get(TAG_ID),observer.getAbsoluteFilePath());      // Store Event ID and Image path for Reverse Lookup after downloading image
                observers.add(observer);

            }



         //   addEventsToHList();       TODO - uncommnet to revert or remove later

            //set on click listeners

            // When the main focused image/video is clicked the app should go to the detail page
            videoView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    //TODO delete one of these Intents. It's redundant
                    Intent detailIntent = new Intent(TrendingActivity.this,EventDetailActivity.class);
                    startActivity(detailIntent);

                    Intent detail = new Intent(getApplicationContext(), EventDetailActivity.class);

                    String event_id = Integer.toString(videoView.getId() );
                    detail.putExtra("event_id",event_id);		//sends the event id to  EventDetailActivity

                    startActivity(detail);
                }
            });
        }

        // @Override
        protected void onCancelled() {
            Toast.makeText(TrendingActivity.this, "Canceled finding out what's going on", Toast.LENGTH_SHORT);
        }
    }

    public void addEventsToHList(){


        Log.d("AddEventsToHList", " adding Events to Horizontal Scroll view...");
        LinearLayout liView = (LinearLayout) findViewById(R.id.hsvLinearLayout);

        ImageView img;

        ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
        DisplayImageOptions options = DisplayImageOptions.createSimple();

        for(HashMap<String,String> event : eventList){         // TODO remove subList function after AWS testing

            img = new ImageView(this);




            // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
            //  which implements ImageAware interface)
           // mImageLoader.displayImage(event.get(TAG_EVENT_IMAGE),img);      // event.get(key) method is using TAG_EVENT_IMAGE to get the image URL


            Picasso.with(this)
               //    .load(event.get(TAG_EVENT_IMAGE))
                    .load(observer.getAbsoluteFilePath())       // Load image from File saved after downloading from AWS S3
                    .resize(600, 600)
                  //  .centerCrop()
                    .into(img);
            // Load image, decode it to Bitmap and return Bitmap synchronously
          //  ImageSize targetSize = new ImageSize(60, 60); // result Bitmap will be fit to this size
          //  Bitmap bmp = imageLoader.loadImageSync(event.get(TAG_EVENT_IMAGE), targetSize, options);
          //  Bitmap bmp = imageLoader.loadImageSync(event.get(TAG_EVENT_IMAGE));

         //   img.setImageBitmap(bmp);        //sets the events image to the image view
            img.setId(observer.getId() );      // TODO remove this test line
     //       img.setId(Integer.parseInt( event.get(TAG_ID) ) );      // Gets the event ID and sets it as the ID of the image.
                                                                    // Later when clicking on the event we can retreive the ID and use to get the event in the next view/detail view

            // RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)img.getLayoutParams();
           // ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            //  params.width = 200;
            // params.height = 200;
            // params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            // params.addRule(RelativeLayout.CENTER_HORIZONTAL);
            params.setMargins(15, 15, 5, 5);
            img.setLayoutParams(params);
            // params.addRule(RelativeLayout.LEFT_OF, R.id.id_to_be_left_of);           // keep this as an example in case needed in future

            /* img attributes

              android:layout_width="200dp"
                android:layout_height="200dp"
                android:id="@+id/imageView"
                android:layout_alignParentBottom="true"
                android:layout_centerHorizontal="true"
                android:layout_marginBottom="10dp"
                android:src="@drawable/trending"
                android:layout_margin="1dp"

             */
            img.setOnClickListener(getOnClickUpdateFocusedEvent(img));      // sets on click listener to update the bigger event image that's the main focus

            liView.addView(img);         // Adds the image to our Linear Layout which is inside of our bottom Horizontal scroll view

        }
    }


    public void addEventsToHList2(int transferObserverId){


        // Get the Transfer Observer for the finished TransferObserver file
        TransferObserver myObserver = awsHelper.getTransferUtility().getTransferById(transferObserverId);
        Log.d("AddEventsToHList", " adding Events to Horizontal Scroll view...");
        LinearLayout liView = (LinearLayout) findViewById(R.id.hsvLinearLayout);

        ImageView img;

        ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
        DisplayImageOptions options = DisplayImageOptions.createSimple();

        String eventId = eventsToFilePathMap.getKey(myObserver.getAbsoluteFilePath());      // Get the event ID corresponding to file to download

        HashMap<String,String> myEvent = new HashMap<String,String>();
        for(HashMap<String,String> event : eventList) {         // find event object values

            if (event.get(TAG_ID) == eventId) {
                myEvent = event;
            }
        }
            // Only load the event image/file from the observer
          //  if(myObserver.getAbsoluteFilePath() != event.get("fileName")){
            //    continue;
            //}
            img = new ImageView(this);

            // The file where we store our image/video locally after download.
            File fileToLoadIntoView = new File(myEvent.get("fileName"));

            // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
            //  which implements ImageAware interface)
            // mImageLoader.displayImage(event.get(TAG_EVENT_IMAGE),img);      // event.get(key) method is using TAG_EVENT_IMAGE to get the image URL

            Log.d("PicassoLoad: ", "Load event id "+myEvent.get("id") + ", file: " + myEvent.get("fileName") );
            Picasso.with(this)
                    //    .load(event.get(TAG_EVENT_IMAGE))
                    .load(fileToLoadIntoView)       // Load image from File saved after downloading from AWS S3
                    .resize(600, 600)
                    //  .centerCrop()
                    .into(img);

            //   img.setImageBitmap(bmp);        //sets the events image to the image view
                  img.setId(Integer.parseInt( myEvent.get(TAG_ID) ) );      // Gets the event ID and sets it as the ID of the image.
            // Later when clicking on the event we can retreive the ID and use to get the event in the next view/detail view

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(15, 15, 5, 5);
            img.setLayoutParams(params);


            img.setOnClickListener(getOnClickUpdateFocusedEvent(img));      // sets on click listener to update the bigger event image that's the main focus

            liView.addView(img);         // Adds the image to our Linear Layout which is inside of our bottom Horizontal scroll view


    }


    // initial image loader for Universal Image Loader library
    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }


    // function for when the user clicks on an event image in the horizontal scroll view
    View.OnClickListener getOnClickUpdateFocusedEvent(final ImageView eventImage)  {
        return new View.OnClickListener() {
            public void onClick(View v) {
               int eventId = eventImage.getId();        // get Event ID from image

                //get event image from list of events, TODO : come back and do a URL get for an event with this id

                // TODO instead of looping through this see if I can just get/select the event with the selected eventId
                // to do this, I would need to pass the index of the event to the function. So it's Index inside eventList


                for(HashMap<String,String> event : eventList){
                    if(Integer.parseInt( event.get(TAG_ID) ) == eventId){
                        Context context = getApplicationContext();

                        File fileToImage = new File(event.get("fileName"));

                        Picasso.with(context)
                                .load(fileToImage)
                                .resize(videoView.getMeasuredWidth(), videoView.getMeasuredHeight())        // resize the image to the heigh and width of temporary placeholder
                                        //  .centerCrop()
                                .into(videoView);

                        videoView.setId(eventId);

                        // Set title for event
                        String event_title = event.get(TAG_TITLE);

                        event_title = event_title.substring(0,Math.min(event_title.length(),20));       // shortens the title to below 20 characters if it's above 20 characters

                        txt_title.setText(event_title);

                        // Get the number of views for the event
                        String arr = event.get(TAG_VIEWS);
                        String[] items = arr.replaceAll("\\[", "").replaceAll("\\]", "").split(",");

                        int[] view_results = new int[items.length];

                        for (int i = 0; i < items.length; i++) {
                            try {
                                view_results[i] = Integer.parseInt(items[i]);
                            } catch (NumberFormatException nfe) {};
                        }
                        txt_views.setText(Integer.toString(view_results.length));

                    }
                }

            }
        };
    }



    // Show Dialog for location
    private void showLocationDialog() {
        FragmentManager fm = getFragmentManager();
        LocationDialog locationDialog = new LocationDialog();
        locationDialog.show(fm, "fragment_set_location");
    }

    // Overrides LocationDialogListener to run this function after user clicks 'done' when searching for a location
    @Override
    public void onFinishEditDialog(String inputText) {
        Toast.makeText(this, "Glad you're at " + inputText, Toast.LENGTH_SHORT).show();
    }



    private void updateList() {
        TransferObserver observer = null;
        HashMap<String, Object> map = null;
        for (int i = 0; i < observers.size(); i++) {
            observer = observers.get(i);
            map = transferRecordMaps.get(i);
            awsHelper.fillMap(map, observer);
        }
        simpleAdapter.notifyDataSetChanged();
    }


    /* A TransferListener class that can listen to a download task and be
    * notified when the status changes.
    */
    private class MyDownloadListener implements TransferListener {
        // Simply updates the list when notified.

        String TAG = "Trending Activity: ";
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "onError: " + id, e);
         //   updateList();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
          //  updateList();
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d(TAG, "onStateChanged: " + id + ", " + state);
            if (state.toString().equals("COMPLETED")) {
                Log.d(TAG, " Download complete. Adding images to screen for event id: " + id);
                addEventsToHList2(id);
            }
        }
    }
}
