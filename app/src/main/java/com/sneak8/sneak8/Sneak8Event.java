package com.sneak8.sneak8;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adrian on 12/15/15.
 */

// Common functions to interact with Sneak8Events and Sneak8 event API

public class Sneak8Event {

    static String username = "adrian";
    static String mPassword = "denver10";

    // Sneak8 Event attributes
    private String event_id;
    private String eventBusiness_id;
    private String title;
    private String event_date;
    private String startTime;
    private String endTime;
    private String description;
    private String views;
    private String price;
    private String photo_url;


    //Sneak8 empty Constructor
    public Sneak8Event(){

    }
    // Sneak8 Event Constructor
    public Sneak8Event(String event_id){
        try {
            JSONObject theEvent = getSingleEvent(event_id);
            this.event_id = theEvent.getString("id");
            this.eventBusiness_id = theEvent.getString("business");
            this.title = theEvent.getString("title");
            this.event_date = theEvent.getString("event_date");
            this.startTime = theEvent.getString("startTime");
            this.endTime = theEvent.getString("endTime");
            this.description = theEvent.getString("description");
            this.views = theEvent.getString("views");
            this.price = theEvent.getString("price");
            this.photo_url = theEvent.getString("photo");


        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }

    }

    // Getters
    public String getEvent_id() {
        return event_id;
    }

    public String getEventBusiness_id() {
        return eventBusiness_id;
    }

    public String getTitle() {
        return title;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getDescription() {
        return description;
    }

    public String getViews() {
        return views;
    }

    public String getPrice() {
        return price;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public static JSONObject getSingleEvent(String event_id){

       //TODO get usenarme and password, remove hardcoded username and password

        String username = "adrian";
        String mPassword = "denver10";
        JSONObject theEvent = null;
        String getSingleEventApiURL = "/events/~api/v1/search/?format=json";
        getSingleEventApiURL += "&id="+event_id;

        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {

            String allData = HttpObject.httpMakeRequest("GET",getSingleEventApiURL,null);
            JSONObject allDataJSON = new JSONObject(allData);
            Log.d("single event count: ", allDataJSON.getString("count"));
            JSONArray eventResults = allDataJSON.getJSONArray("results");    // Gets all of the results/ events


            theEvent = eventResults.getJSONObject(0);
            if(theEvent != null) {
                System.out.println("Event with id " + event_id + ": " + theEvent);
            }else
            {
                System.out.println("Could not retrieve event with id " + event_id);
            }

           // httpCon.disconnect();
        } catch(Exception e){
            Log.d("Err:getSingleEvent"," See below for stack trace");
           e.printStackTrace();
        }


        return theEvent;        // Returns the JSON event requested
    }


    public static JSONArray getCommentsForEvent(String event_id){

        //TODO get usenarme and password, remove hardcoded username and password

        String username = "adrian";
        String mPassword = "denver10";
        JSONArray eventComments = null;
        String getCommentsApiURL = "/events/comments/~api/v1/search/?format=json";
        getCommentsApiURL += "&event="+event_id;

        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {

            String allData = HttpObject.httpMakeRequest("GET",getCommentsApiURL,null);
            JSONObject allDataJSON = new JSONObject(allData);
             eventComments = allDataJSON.getJSONArray("results");  // Gets the comments that was returned.


            if(eventComments != null) {
                System.out.println("Comments with Event with id " + event_id + ": " + eventComments);
            }else
            {
                System.out.println("Could not retrieve Comments event with id " + event_id);
            }

           // httpCon.disconnect();
        } catch(Exception e){
            Log.d("Err:getComments"," See below for stack trace");
            e.printStackTrace();
        }


        return eventComments;        // Returns the JSON event requested
    }

    public static String getCommentUsername(String user_id){
        String commentUser = null;
        JSONObject theUser = null;
        String getCommentApiURL = "/users/~api/v1/search/?format=json";
        getCommentApiURL += "&id="+user_id;;
        try {

            String allData = HttpObject.httpMakeRequest("GET",getCommentApiURL,null);
            JSONObject allDataJSON = new JSONObject(allData.toString());
            JSONArray UserList = allDataJSON.getJSONArray("results");  // Gets the comments that was returned.

            theUser = UserList.getJSONObject(0);
            commentUser = theUser.getString("username");        // get the username

            if(UserList != null) {
                System.out.println("Comment with User with id " + user_id + ": " + UserList);
            }else
            {
                System.out.println("Could not retrieve the User with id " + user_id);
            }

        } catch(Exception e){
            Log.d("Err:getCommentUsername:"," See below for stack trace");
            e.printStackTrace();
        }

        return  commentUser;

    }



    public static boolean createEventComment(String user_id,String event_id,String content){


        try {
            String commentURL = TrendingActivity.BASE_URL +"/events/comments/~api/v1/create/";

            String userIdParam = "user_id=";
            String eventIdParam = "event_id=";
            String contentParam = "content=";

            String urlParameters = (userIdParam+user_id) +"&"+ (eventIdParam+event_id) + "&" + (contentParam+content);
            urlParameters = "user_id="+user_id +"&"+"event_id="+event_id+"&"+"content="+content;
            Log.d("Event_id:",event_id);
            Log.d("Content posted:", content);
            if(user_id != null)
                Log.d("urlParams: ", urlParameters);
            else
            {
                Log.d("error: " , "user id was not recieved");
                return false;
            }
            Log.d("User URL: " , commentURL);
            URL url = new URL(commentURL);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setUseCaches(false);
            httpCon.setAllowUserInteraction(false);
            httpCon.setDoOutput(true);
            httpCon.setChunkedStreamingMode(0);
            httpCon.setRequestMethod("POST");
           // httpCon.setRequestProperty("Content-Type",
             //       "application/x-www-form-urlencoded");
            httpCon.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));


            String userPassword = username + ":" + mPassword;
            String encoding = Base64.encodeToString(userPassword.getBytes(), Base64.DEFAULT);


            httpCon.addRequestProperty("Authorization", "Basic " + encoding);
           // httpCon.setFixedLengthStreamingMode(
            //        urlParameters.getBytes().length);

            PrintWriter out = new PrintWriter(httpCon.getOutputStream());
            out.print(urlParameters);
            out.close();


            int statusCode =  httpCon.getResponseCode();
            System.out.println("Comment Status code: " + statusCode);

            if(statusCode == HttpURLConnection.HTTP_OK){
                httpCon.disconnect();
                return true;
            }
            else {
                httpCon.disconnect();
                return  false;
            }


        } catch (Exception e) {
            Log.d("Err:postEventComment:"," See below for stack trace");
            e.printStackTrace();
            return  false;
        }



    }


    // Search for event. used in SneakPeak
    public static ArrayList<Sneak8Event> SneakPeakSearch(JSONObject searchTerms){

        //TODO get usenarme and password, remove hardcoded username and password
        // TODO implement specials

        String username = "adrian";
        String mPassword = "denver10";
        String attribute = "title";


        JSONArray searchedResults = null;
        String eventData = null;

        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {
            // Sneak Search api URL. Send post data
            // Send JSON object with "title" key and search values to search through titles
            String sneakSearchUrl ="/events/~api/v1/sneaksearch/";
            eventData = HttpObject.httpMakeRequest("POST",sneakSearchUrl,searchTerms);
             searchedResults = new JSONArray(eventData);
           // searchedResults = allData.getJSONArray("results");
        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }


        return  jsonArrayToSneak8ArrayList(searchedResults);
    }



    public static ArrayList<Sneak8Event> getSuggestedEvents(){

    Log.d("Location: ", "Sneak8Event.getSuggestedEvents()");
    ArrayList<Sneak8Event> suggestedEvents = new ArrayList<Sneak8Event>();
    JSONArray eventsArray = null;
    String allData = null;

    String apiURL = "/events/getsuggested/~api/v1/?format=json";

    String requestMethod = "GET";       // TODO change to "POST" once changed on server side

    allData = HttpObject.httpMakeRequest(requestMethod, apiURL, null);

    try {
        JSONObject allDataJSON = new JSONObject(allData.toString());

        eventsArray = allDataJSON.getJSONArray("results");

    }catch (JSONException e){
        e.getMessage();
        e.printStackTrace();
    }

        suggestedEvents = jsonArrayToSneak8ArrayList(eventsArray);
        Log.d("Sneak8EventArrayList:",suggestedEvents.toString());

    return  suggestedEvents;
}

    private static ArrayList<Sneak8Event> jsonArrayToSneak8ArrayList(JSONArray jsonArray){

        ArrayList<Sneak8Event> sneak8ArrayList = new ArrayList<Sneak8Event>();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonEvent = jsonArray.getJSONObject(i);
                Sneak8Event tempEvent = new Sneak8Event(jsonEvent.getString("id"));
                sneak8ArrayList.add(tempEvent);

                Log.d("JsonArray to list: ", " Successful in translating JSONArray to Sneak8Event ArrayList");
            }
        }catch (JSONException e){
            Log.d("jsonArray to list: ", " Error translating JSONArray to Sneak8Event Arraylist");

            e.getMessage();
            e.printStackTrace();
        }


        return sneak8ArrayList;

    }

    @Override
    public String toString(){

        return "Event Title: " + this.title;
    }


}