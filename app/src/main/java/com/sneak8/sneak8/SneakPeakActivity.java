package com.sneak8.sneak8;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by adrian on 1/4/16.
 */
public class SneakPeakActivity extends Activity implements OnClickListener {


    private ArrayList<Sneak8Event> suggestedEvents;
    private ArrayList<Sneak8Event> searchResults;
    private ImageView btnHome;
    private ImageView btnSneakPeak;
    private ImageView btnProfile;
    private View navBar;
    private EditText edtSearchBox;
    private ProgressBar mProgressBar;        // progress bar to show while loading event
    LinearLayout[] imageContainers = new LinearLayout[6];       // Create 6 image containers for the 6 suggested events shown.
    String searchTerm = " ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sneakpeak);


        Spinner dropdown = (Spinner) findViewById(R.id.sneaksearch_spinner);
        String[] items = new String[]{"Place name", "Specials"};        // list of items in spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(adapter);
        dropdown.setSelection(0);       // sets the spinner to the first selection, which is "Place name"


        navBar = findViewById(R.id.sneakpeak_navBar);
        btnHome = (ImageView) navBar.findViewById(R.id.btnHome);
        btnSneakPeak = (ImageView) navBar.findViewById(R.id.btnSneakPeak);
        btnProfile = (ImageView) navBar.findViewById(R.id.btnProfile);
        mProgressBar = (ProgressBar) findViewById(R.id.sneakpeak_ProgressBar);
        edtSearchBox = (EditText) findViewById(R.id.sneakpeak_searchbox);

        Log.d("imageContainers Length:", Integer.toString(imageContainers.length));
        // set image containers
        imageContainers[0] = (LinearLayout) findViewById(R.id.sneakpeak_suggestion1);
        imageContainers[1] = (LinearLayout) findViewById(R.id.sneakpeak_suggestion2);
        imageContainers[2] = (LinearLayout) findViewById(R.id.sneakpeak_suggestion3);
        imageContainers[3] = (LinearLayout) findViewById(R.id.sneakpeak_suggestion4);
        // imageContainers[4] = (LinearLayout) findViewById(R.id.sneakpeak_suggestion5);
        //  imageContainers[5] = (LinearLayout) findViewById(R.id.sneakpeak_suggestion6);


        btnHome.setOnClickListener(this);
        btnSneakPeak.setOnClickListener(this);
        btnProfile.setOnClickListener(this);

        new LoadSuggestedEvents().execute(0);

    }


    @Override
    public void onClick(View v) {

        // Navigate to home page
        if (v == btnHome) {
            Intent home = new Intent(SneakPeakActivity.this, TrendingActivity.class);
            startActivity(home);
        }

        // Navigate to Trending page
        if (v == btnSneakPeak) {
            Intent home = new Intent(SneakPeakActivity.this, SneakPeakActivity.class);
            startActivity(home);
        }

        // Navigate to Profile page     // TODO create User Profile activity and navigate to it
        if (v == btnProfile) {
            // Intent profile = new Intent(EventDetailActivity.this,UserProfileActivity.class);
            // startActivity(profile);
            searchTerm = edtSearchBox.getText().toString();
            new LoadSuggestedEvents().execute(1);
        }


    }

    public class LoadSuggestedEvents extends AsyncTask<Integer, Void, Void> {

        boolean usedSearch;         // boolean to determine if user made a search or not

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);

        }


        @Override
        protected Void doInBackground(Integer... params) {


            try {
                if (params[0] == 1) {
                    JSONObject searchParameters = new JSONObject();
                    searchParameters.put("title", searchTerm);
                    Log.d("Before Searching : " , ".. Searching");
                    searchResults = Sneak8Event.SneakPeakSearch(searchParameters);
                    usedSearch = true;
                    System.out.println(" Returned Searched data: " + searchResults.toString());
                } else {
                    suggestedEvents = Sneak8Event.getSuggestedEvents();
                    usedSearch = false;
                    System.out.println(" returned suggestions: " + suggestedEvents);
                }

            } catch (Exception e) {
               Log.d("Error searching: ", e.getMessage() );
               e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressBar.setVisibility(View.GONE);

            Log.d("Used Search? : " , String.valueOf(usedSearch) );
            if (!usedSearch) {

                try {
                    Log.d("suggested events size:", Integer.toString(suggestedEvents.size()));
                    Log.d("SneakPeakActivity: ", "in post execute");
                    // Loop through suggested events, update UI
                    for (int i = 0; i < suggestedEvents.size(); i++) {

                        Log.d("suggestion loop: ", "in post execute");

                        Sneak8Event event = suggestedEvents.get(i);


                        final ImageView eventImage;
                        eventImage = (ImageView) imageContainers[i].getChildAt(0);

                        // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
                        //  which implements ImageAware interface)
                        // mImageLoader.displayImage(event.get(TAG_EVENT_IMAGE),img);      // event.get(key) method is using TAG_EVENT_IMAGE to get the image URL

                        System.out.println("Suggested event: " + event);
                        System.out.println("event photo url: " + event.getPhoto_url());
                        Picasso.with(getApplicationContext())
                                .load(event.getPhoto_url())
                                //      .resize(150, 100)         uncomment this line if image does not fit into container
                                //  .centerCrop()
                                .into(eventImage);

                        eventImage.setId(Integer.parseInt(event.getEvent_id()));      // Gets the event ID and sets it as the ID of the image.
                        // Later when clicking on the event we can retreive the ID and use to get the event in the next view/detail view

                        // Set onClick Listener to get to event detail page when clicked
                        eventImage.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent detail = new Intent(getApplicationContext(), EventDetailActivity.class);

                                String event_id = Integer.toString(eventImage.getId());
                                detail.putExtra("event_id", event_id);        //sends the event id to  EventDetailActivity

                                startActivity(detail);
                            }
                        });
                    }

                } catch (Exception e) {
                    Log.d("Error-SneakPeak:", e.getMessage());
                    e.printStackTrace();
                }

            }

            if (usedSearch) {


                LinearLayout liView = (LinearLayout) findViewById(R.id.LinearEventContainer);

                // Clear the List of previous suggested events
                liView.removeAllViews();
                liView.invalidate();

                ImageView img;

                ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
                DisplayImageOptions options = DisplayImageOptions.createSimple();

                try {


                    for (Sneak8Event event: searchResults) {


                        img = new ImageView(getApplicationContext());


                        // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
                        //  which implements ImageAware interface)
                        // mImageLoader.displayImage(event.get(TAG_EVENT_IMAGE),img);      // event.get(key) method is using TAG_EVENT_IMAGE to get the image URL



                        Picasso.with(getApplicationContext())
                                .load(event.getPhoto_url())
                                .resize(600, 600)
                                //  .centerCrop()
                                .into(img);


                        img.setId(Integer.parseInt(event.getEvent_id()));      // Gets the event ID and sets it as the ID of the image.
                        // Later when clicking on the event we can retreive the ID and use to get the event in the next view/detail view


                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        params.setMargins(15, 15, 5, 5);
                        img.setLayoutParams(params);

                        img.setOnClickListener(getOnClickUpdateFocusedEvent(img));      // sets on click listener to update the bigger event image that's the main focus

                        liView.addView(img);         // Adds the image to our Linear Layout which is inside of our bottom Horizontal scroll view

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


        }    // End of Post Execute


    }       // end of load suggested events

    View.OnClickListener getOnClickUpdateFocusedEvent(final ImageView eventImage){

        return new View.OnClickListener() {
            public void onClick(View v) {
                int eventId = eventImage.getId();        // get Event ID from image

                Intent goToEvent = new Intent(SneakPeakActivity.this,EventDetailActivity.class);


                String event_id = Integer.toString(eventId );
                goToEvent.putExtra("event_id",event_id);		//sends the event id to  EventDetailActivity

                startActivity(goToEvent);
            }
        };
    }
}
