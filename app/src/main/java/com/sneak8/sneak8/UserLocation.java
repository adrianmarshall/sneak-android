package com.sneak8.sneak8;

/**
 * Created by adrian on 4/9/17.
 */

public class UserLocation {

    String city = "";
    String state = "";
    String zipcode ="";
    String longitude ="";
    String latitude = "";
    String addressline1 = "";
    String addressline2 = "";


    @Override
    public String toString(){

        String location = addressline1 + "\n" + addressline2 + "\n" + city + "," + state + zipcode;
        return location;
    }
}
