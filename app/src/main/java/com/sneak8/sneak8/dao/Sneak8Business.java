package com.sneak8.sneak8.dao;

import android.util.Log;

import com.sneak8.sneak8.HttpObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by adrian on 12/15/15.
 */

// Common functions to interact with Sneak8Events and Sneak8 event API

public class Sneak8Business {

   

    // Sneak8 Business attributes
    private String business_id;
    private String owner;
    private String name;
    private String AddressLine1;
    private String AddressLine2;
    private String city;
    private String state;
    private String zipcode;
    private String phone_number;


    //Sneak8 empty Constructor
    public Sneak8Business(){

    }
    // Sneak8 Event Constructor
    public Sneak8Business(String business_id){
        try {
            JSONObject theBusiness = getSingleBusiness(business_id);
            this.business_id = theBusiness.getString("id");
            this.owner = theBusiness.getString("owner");
            this.name = theBusiness.getString("name");
            this.AddressLine1 = theBusiness.getString("Addressline1");
            this.AddressLine2 = theBusiness.getString("Addressline2");
            this.city = theBusiness.getString("city");
            this.state = theBusiness.getString("state");
            this.zipcode = theBusiness.getString("zipcode");
            this.phone_number = theBusiness.getString("phone_number");


        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }

    }

   

    public static JSONObject getSingleBusiness(String business_id){

       //TODO get usenarme and password, remove hardcoded username and password

        JSONObject theBusiness = null;
        String getSingleEventApiURL = "/businesses/~api/v1/search/?format=json";
        getSingleEventApiURL += "&id="+business_id;

        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {

            String allData = HttpObject.httpMakeRequest("GET",getSingleEventApiURL,null);
            JSONObject allDataJSON = new JSONObject(allData);
            Log.d("single business count: ", allDataJSON.getString("count"));
            JSONArray eventResults = allDataJSON.getJSONArray("results");    // Gets all of the results/ events


            theBusiness = eventResults.getJSONObject(0);
            if(theBusiness != null) {
                System.out.println("Business with id " + business_id + ": " + theBusiness);
            }else
            {
                System.out.println("Could not retrieve business with id " + business_id);
            }

           // httpCon.disconnect();
        } catch(Exception e){
            Log.d("Err:getSingleBusiness"," See below for stack trace");
           e.printStackTrace();
        }


        return theBusiness;        // Returns the JSON event requested
    }


    public static JSONArray getCommentsForEvent(String event_id){

        //TODO get usenarme and password, remove hardcoded username and password

        String username = "adrian";
        String mPassword = "denver10";
        JSONArray eventComments = null;
        String getCommentsApiURL = "/events/comments/~api/v1/search/?format=json";
        getCommentsApiURL += "&event="+event_id;

        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {

            String allData = HttpObject.httpMakeRequest("GET",getCommentsApiURL,null);
            JSONObject allDataJSON = new JSONObject(allData);
             eventComments = allDataJSON.getJSONArray("results");  // Gets the comments that was returned.


            if(eventComments != null) {
                System.out.println("Comments with Event with id " + event_id + ": " + eventComments);
            }else
            {
                System.out.println("Could not retrieve Comments event with id " + event_id);
            }

           // httpCon.disconnect();
        } catch(Exception e){
            Log.d("Err:getComments"," See below for stack trace");
            e.printStackTrace();
        }


        return eventComments;        // Returns the JSON event requested
    }

    public static String getCommentUsername(String user_id){
        String commentUser = null;
        JSONObject theUser = null;
        String getCommentApiURL = "/users/~api/v1/search/?format=json";
        getCommentApiURL += "&id="+user_id;;
        try {

            String allData = HttpObject.httpMakeRequest("GET",getCommentApiURL,null);
            JSONObject allDataJSON = new JSONObject(allData.toString());
            JSONArray UserList = allDataJSON.getJSONArray("results");  // Gets the comments that was returned.

            theUser = UserList.getJSONObject(0);
            commentUser = theUser.getString("username");        // get the username

            if(UserList != null) {
                System.out.println("Comment with User with id " + user_id + ": " + UserList);
            }else
            {
                System.out.println("Could not retrieve the User with id " + user_id);
            }

        } catch(Exception e){
            Log.d("Err:getCommentUsername:"," See below for stack trace");
            e.printStackTrace();
        }

        return  commentUser;

    }

    


    // Search for event. used in SneakPeak
    public static JSONArray SneakPeakSearch(JSONObject searchTerms){

        //TODO get usenarme and password, remove hardcoded username and password
        // TODO implement specials

        String username = "adrian";
        String mPassword = "denver10";
        String attribute = "title";


        JSONArray searchedResults = null;
        String eventData = null;

        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {
            // Sneak Search api URL. Send post data
            // Send JSON object with "title" key and search values to search through titles
            String sneakSearchUrl ="/events/~api/v1/sneaksearch/";
            eventData = HttpObject.httpMakeRequest("POST",sneakSearchUrl,searchTerms);
            JSONObject allData = new JSONObject(eventData.toString());
            searchedResults = allData.getJSONArray("results");
        }catch (Exception e){
            e.getMessage();
        }


        return  searchedResults;
    }



    public static ArrayList<Sneak8Business> getSuggestedEvents(){

    Log.d("Location: ", "Sneak8Event.getSuggestedEvents()");
    ArrayList<Sneak8Business> suggestedEvents = new ArrayList<Sneak8Business>();
    JSONArray eventsArray = null;
    String allData = null;

    String apiURL = "/events/getsuggested/~api/v1/?format=json";

    String requestMethod = "GET";       // TODO change to "POST" once changed on server side

    allData = HttpObject.httpMakeRequest(requestMethod, apiURL, null);

    try {
        JSONObject allDataJSON = new JSONObject(allData.toString());

        eventsArray = allDataJSON.getJSONArray("results");

    }catch (JSONException e){
        e.getMessage();
        e.printStackTrace();
    }

        suggestedEvents = jsonArrayToSneak8ArrayList(eventsArray);
        Log.d("Sneak8EventArrayList:",suggestedEvents.toString());

    return  suggestedEvents;
}

    private static ArrayList<Sneak8Business> jsonArrayToSneak8ArrayList(JSONArray jsonArray){

        ArrayList<Sneak8Business> sneak8ArrayList = new ArrayList<Sneak8Business>();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonEvent = jsonArray.getJSONObject(i);
                Sneak8Business tempEvent = new Sneak8Business(jsonEvent.getString("id"));
                sneak8ArrayList.add(tempEvent);

                Log.d("JsonArray to list: ", " Successful in translating JSONArray to Sneak8Event ArrayList");
            }
        }catch (JSONException e){
            Log.d("jsonArray to list: ", " Error translating JSONArray to Sneak8Event Arraylist");

            e.getMessage();
            e.printStackTrace();
        }


        return sneak8ArrayList;

    }

    @Override
    public String toString(){

        return "Business name: " + this.name;
    }


}