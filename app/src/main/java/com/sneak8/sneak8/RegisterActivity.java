package com.sneak8.sneak8;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.StringBuilderPrinter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sneak8.sneak8.R;

public class RegisterActivity extends Activity implements OnClickListener {

    Sneak8User newUser = null;  // user to be created

    // UI fields on the activity_register page
   private EditText edt_FirstName;
   private EditText edt_LastName;
   private EditText edt_Username;
   private EditText edt_Email;
   private EditText edt_Password;
   private EditText edt_ConfirmPassword;

   private Button btn_Submit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edt_FirstName = (EditText) findViewById(R.id.register_FirstName);
        edt_LastName = (EditText) findViewById(R.id.register_LastName);
        edt_Username = (EditText) findViewById(R.id.register_username);
        edt_Email = (EditText) findViewById(R.id.register_EmailAddress);
        edt_Password = (EditText) findViewById(R.id.register_Password);
        edt_ConfirmPassword = (EditText) findViewById(R.id.register_confirmPassword);

        btn_Submit = (Button) findViewById(R.id.btn_register_submit);

        btn_Submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v.equals(btn_Submit)){
            String firstName = edt_FirstName.getText().toString();
            String lastName = edt_LastName.getText().toString();
            String username = edt_Username.getText().toString();
            String email = edt_Email.getText().toString();
            String password = edt_Password.getText().toString();
            String confirmPassword = edt_ConfirmPassword.getText().toString();

            // check if passwords match
            if(!password.equals(confirmPassword)){
                Toast.makeText(RegisterActivity.this, "Passwords do not match. Re-Enter", Toast.LENGTH_SHORT).show();
            }else {
                // Register new user
                RegisterUserTask mNewRegistrationTask;
                mNewRegistrationTask = new RegisterUserTask(firstName,lastName,username,email,password,confirmPassword);
                mNewRegistrationTask.execute((Void)null);
            }
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class RegisterUserTask extends AsyncTask<Void, Void, Boolean> {

        private final String firstName;
        private final String lastName;
        private final String username;
        private final String email;
        private final String password;
        private final String confirmPassword;

        RegisterUserTask(String firstName,String lastName, String username,String email,
                      String password,String confirmPassword) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.username = username;
            this.email = email;
            this.password = password;
            this.confirmPassword = confirmPassword;
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            String type = "regular";        // this indicates we are creating a regular user(non-business)
             newUser = new Sneak8User(firstName,lastName,username,email,type,password);
            if(newUser == null){
                Toast.makeText(getApplicationContext(), "Error creating user", Toast.LENGTH_LONG);
            }else {
                Toast.makeText(getApplicationContext(), "New user: " + newUser.getUsername() + " created", Toast.LENGTH_LONG);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            SharedPreferences sharedPreferences;		// shared preferences will be used to store username & password information
            final String MyPREFERENCES = "MyPrefs" ;

            String usernameKey = "username";
            String passwordKey = "password";

            sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

            if(newUser != null) {

                // Save user's username and password to shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString(usernameKey, username);    // Adding the username to the preferences
                editor.putString(passwordKey, password);    // Adding the password to the preferences
                //	editor.putString("id",users.getJSONObject(0).getString("id"));	// Adding the users id

                editor.commit();    // Commits (saves) the username and password data we just added

                // Go to Login Activity
                // Note: If the users credentials are stored teh Login Activity automatically logs the user in
                Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                Toast.makeText(getApplicationContext(), "Logging you in!", Toast.LENGTH_SHORT);
                startActivity(loginIntent);

            }else{
                Toast.makeText(getApplicationContext(),"Error creating user. Try again",Toast.LENGTH_LONG);
            }
        }


    }


    public void BackOnClick(Button Back) {
        //Does something when clicked
        Back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_login);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //  if (id == R.id.action_) {
        //    return true;
        // }

        return super.onOptionsItemSelected(item);
    }

}

