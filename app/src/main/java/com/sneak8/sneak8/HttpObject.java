package com.sneak8.sneak8;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by adrian on 1/19/16.
 */
public class HttpObject extends Activity {

    static String username = LoginActivity.username;
    static String mPassword = LoginActivity.password;

    String usernameKey = "username";
    String passwordKey = "password";

    SharedPreferences sharedPreferences;		// shared preferences will be used to store username & password information

    public static final String MyPREFERENCES = "MyPrefs" ;

    // endpoints
    static String createUserApiURL = "/users/~api/v1/";

/*
    public HttpObject(){

        // Restore preferences
        SharedPreferences settings = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        // If the user's username and password is in the shared preferences then we will log them in
        if(sharedPreferences.contains(usernameKey) && sharedPreferences.contains(passwordKey)){
            try{
                username = sharedPreferences.getString(usernameKey,"");
                mPassword = sharedPreferences.getString(passwordKey,"");
            }catch (Exception e){
                e.printStackTrace();
            }
        }



    } */


    public static String httpMakeRequest(String method,String apiURL,JSONObject parameters){




        String theData = null;
        Log.d("Location: ", "Sneak8Event.httMakeRequest Start");
        if(method == "GET") {
            Log.d("Location: ", "Sneak8Event.httMakeRequest GET");

            String urlAddress;
            try {
                // String addr = "http://10.0.0.5:8000/events/~api/v1/search/";        // Search/filter api for events
                if(apiURL.startsWith("http")){      //if a base URL is already provided (i.e- accessing another server)
                    urlAddress = apiURL;
                }else{
                     urlAddress = TrendingActivity.BASE_URL + apiURL;
                }


                if(parameters != null) {
                    // Build parameters
                    Iterator<String> keySet = parameters.keys();

                    while (keySet.hasNext()) {
                        String key = keySet.next();     // gets the current JSON key
                        String value = parameters.getString(key);
                        urlAddress += "&" + key + "=" + value;
                    }

                }
                /* build parameters
                if (parameters != null) {
                    for (Map.Entry<String, String> entry : parameters ) {

                        urlAddress += "&" + entry.getKey() + "=" + entry.getValue();
                    }
                }
                */

                Log.d(" URL Address: ", urlAddress);
                URL url = new URL(urlAddress);
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                httpCon.setUseCaches(false);
                httpCon.setAllowUserInteraction(false);
                String userPassword = username + ":" + mPassword;
                String encoding = Base64.encodeToString(userPassword.getBytes(), Base64.DEFAULT);
                httpCon.addRequestProperty("Authorization", "Basic " + encoding);

                System.out.println(httpCon.getResponseCode());

                // Get the data returned
                StringBuilder results = new StringBuilder();
                InputStream in = new BufferedInputStream(httpCon.getInputStream());     // Get input stream

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));  // read in the data from input stream

                String line = "";

                // loop through the data and build the string of results from the http request.
                while ((line = reader.readLine()) != null) {
                    results.append(line);
                }

                System.out.println("results:" + results.toString());     // for dev/test purposes, output the results


                //JSONObject allData = new JSONObject(results.toString());
                theData = results.toString(); // Gets the results that was returned.


                if (theData != null) {
                    System.out.println("Data from server:  " + theData.toString());
                } else {
                    System.out.println("Could not retrieve data for parameters" + parameters.toString());
                }

                httpCon.disconnect();
            } catch (Exception e) {
                Log.d("Er:httpMakeRequest GET:", "See below for stack trace");
                e.printStackTrace();
            }
        }   // end of block for GET method
        else if(method == "POST"){

            if(apiURL == createUserApiURL){
                // if we are about to create a new user, use admin creds
                username = "adrian";        // todo come back to securily add admin creds
                mPassword = "denver10";
            }
            Log.d("Location: ", "Sneak8Event.httMakeRequest POST");
            try {
                // String addr = "http://10.0.0.5:8000/events/~api/v1/search/";        // Search/filter api for events
                String urlAddress = TrendingActivity.BASE_URL + apiURL;
                String urlParameters = "";

                // build parameters
                StringBuilder builder = new StringBuilder(parameters.toString().length());
                int i = 0;



                Log.d(" URL Address: ", urlAddress);
                /*
                URL url = new URL(urlAddress);
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                httpCon.setUseCaches(false);
                httpCon.setAllowUserInteraction(false);
                httpCon.setDoOutput(true);
                httpCon.setDoInput(true);
                httpCon.setChunkedStreamingMode(0);
                httpCon.setRequestMethod("POST");
                httpCon.setRequestProperty("Content-Length", "" +
                        Integer.toString(parameters.toString().getBytes().length));
                httpCon.setRequestProperty("Content-Type", "application/json");
                String userPassword = username + ":" + mPassword;
                String encoding = Base64.encodeToString(userPassword.getBytes(), Base64.DEFAULT);


                httpCon.addRequestProperty("Authorization", "Basic " + encoding);

                Log.d("parameters json: ", parameters.toString());
                PrintWriter out = new PrintWriter(httpCon.getOutputStream());
                out.print(URLEncoder.encode(" { 'title': 'effies' }","UTF-8"));
                out.flush();
                out.close();
                */
                byte[] data = parameters.toString().getBytes();
                URL url;
                HttpURLConnection urlConn;
                DataOutputStream printout;
                DataInputStream input;
                url = new URL (urlAddress);
                urlConn = (HttpURLConnection) url.openConnection();
                String userPassword = username + ":" + mPassword;
                String encoding = Base64.encodeToString(userPassword.getBytes(), Base64.DEFAULT);
                urlConn.addRequestProperty("Authorization", "Basic " + encoding);
                //urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                //urlConn.setUseCaches(false);
                urlConn.setRequestMethod("POST");
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("Accept", "application/json");
                urlConn.connect();

                byte[] outputBytes ="{\"title\":\"edm\"}".getBytes("UTF-8");
                outputBytes = parameters.toString().getBytes("UTF-8");
                OutputStream os = urlConn.getOutputStream();
                os.write(outputBytes);
                os.close();

                int statusCode =  urlConn.getResponseCode();
                System.out.println(" Status code: " + statusCode);
                if(statusCode == 403 || statusCode == 400){
                    System.out.println("Error Stream: " + urlConn.getErrorStream());
                }

                /* Send POST output.
                printout = new DataOutputStream(urlConn.getOutputStream ());
                String postDataString = parameters.toString();
                byte[] postDataBytes = postDataString.getBytes("UTF-8");
                printout.write(postDataBytes);
                printout.flush();
                printout.close();

                */




                //theData = new JSONArray("[{\"statuscode\":"+statusCode+"}]");

               // Get the data returned
                StringBuilder results = new StringBuilder();
                InputStream in = new BufferedInputStream(urlConn.getInputStream());     // Get input stream

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));  // read in the data from input stream

                String line = "";

                // loop through the data and build the string of results from the http request.
                while ((line = reader.readLine()) != null) {
                    results.append(line);
                }

                System.out.println(results.toString());     // for dev/test purposes, output the results

                theData = results.toString();

                urlConn.disconnect();
                return  theData;

            } catch (Exception e) {
                Log.d("Error:post URL call:"," See below for stack trace");
                e.printStackTrace();
            }


        }

        return theData;        // Returns the JSON event requested
    }
}
