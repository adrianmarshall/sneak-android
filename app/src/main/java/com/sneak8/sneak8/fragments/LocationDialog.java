package com.sneak8.sneak8.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.sneak8.sneak8.R;

/**
 * Created by adrian on 1/20/17.
 */

public class LocationDialog extends DialogFragment implements TextView.OnEditorActionListener {

    public interface LocationDialogListener {
        void onFinishEditDialog(String inputText);
    }

    private EditText mSearchLocation;

    public LocationDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_location, container);
        mSearchLocation = (EditText) view.findViewById(R.id.txt_your_location);
        getDialog().setTitle("Hello");

        // Show soft keyboard automatically
        mSearchLocation.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        mSearchLocation.setOnEditorActionListener(this);

        return view;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            // Return input text to activity
            LocationDialogListener activity = (LocationDialogListener) getActivity();
            activity.onFinishEditDialog(mSearchLocation.getText().toString());
            this.dismiss();
            return true;
        }
        return false;
    }
}
