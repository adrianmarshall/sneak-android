package com.sneak8.sneak8.video;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.red5pro.streaming.R5Connection;
import com.red5pro.streaming.R5Stream;
import com.red5pro.streaming.R5Stream.RecordType;
import com.red5pro.streaming.R5StreamProtocol;
import com.red5pro.streaming.config.R5Configuration;
import com.red5pro.streaming.source.R5Camera;
import com.red5pro.streaming.source.R5Microphone;
import com.sneak8.sneak8.R;
import com.sneak8.utility.common;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PublishFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PublishFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PublishFragment extends Fragment implements SurfaceHolder.Callback{


    public R5Configuration configuration;

    protected Camera camera;
    protected boolean isPublishing = false;
    protected R5Stream stream;

    int cameraWidth;
    int cameraHeight;



    public PublishFragment() {
        // Required empty public constructor
    }

    public static PublishFragment newInstance() {
        PublishFragment fragment = new PublishFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources res = getResources();
        configuration = new R5Configuration(R5StreamProtocol.RTSP,res.getString(R.string.domain),8554,
                "live",1.0f);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        ImageButton publishButton = (ImageButton)
                getActivity().findViewById(R.id.publishButton);

        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPublishToggle();
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_publish2, container, false);
        return v;
    }


    private void preview(){

        camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        SurfaceView surface = (SurfaceView) getActivity().findViewById(R.id.surfaceViewFull);
        surface.getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder);
            camera.setDisplayOrientation(90);
            camera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        this.cameraWidth = width;
        this.cameraHeight = height;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        preview();
    }

    private void onPublishToggle(){
        ImageButton publishButton = (ImageButton)
                getActivity().findViewById(R.id.publishButton);

        if(isPublishing){

            stop();
        }else{
            start();
        }
        isPublishing = !isPublishing;
        publishButton.setImageResource(isPublishing ? R.drawable.ic_recordbutton_after:R.drawable.ic_recordbutton_before);
    }

    public void start() {
        camera.stopPreview();

        stream = new R5Stream(new R5Connection(configuration));
        stream.setView((SurfaceView) getActivity().findViewById(R.id.surfaceViewFull));

        // get full screen size of camera
  /*      if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
            Camera.Size size = getOptimalPreviewSize(mSupportedPreviewSizes, cameraWidth, cameraHeight);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);

                camera.setParameters(parameters);
            }
            }
*/
         //   Camera.Parameters parameters = camera.getParameters();
           // Camera.Size theSize =  parameters.getPreviewSize();
           // R5Camera r5Camera = new R5Camera(camera,theSize.height, theSize.width);
            R5Camera r5Camera = new R5Camera(camera,320,240);
            R5Microphone r5Microphone = new R5Microphone();

            stream.attachCamera(r5Camera);
            stream.attachMic(r5Microphone);

          //  String streamID = createStreamID("13eID");
            //TODO remove string literal , use streamID variable
            stream.publish("red5prostream", RecordType.Live);
            camera.startPreview();

          String testResults = common.getAllLiveStreams(); //todo make async becasue this is on main thread. maybe use runnable
        if(testResults == null){
            testResults = "No Streams available";
        }
          Log.d("available Streams: ",testResults);

    }

    public void stop(){
        if(stream != null){
            stream.stop();
            camera.startPreview();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        if(isPublishing){
            onPublishToggle();
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;
        if (sizes == null) {
            return null;
        }
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        for (Camera.Size size : sizes) {
            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
                continue;
            }
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public String createStreamID(String eventID){


        SharedPreferences sharedPreferences;		// shared preferences will be used to store username & password information

        final String MyPREFERENCES = "MyPrefs" ;

        String usernameKey = "username";
        String username = "";
        String streamID = "";

        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        // If the user's username and password is in the shared preferences then we will log them in
        if(sharedPreferences.contains(usernameKey)){
            try{
                username = sharedPreferences.getString(usernameKey, "");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        /**
         * Format that the Event Stream will be created is "<EventID> -<Username>"
         * Note that the eventID and username are separated by a hyphen. this can be parsed out later
         */
        streamID = eventID +"-"+username;
        Log.d("StreamID:",streamID);

        return streamID;
    }
}
