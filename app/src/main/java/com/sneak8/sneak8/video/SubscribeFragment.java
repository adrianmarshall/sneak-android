package com.sneak8.sneak8.video;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.red5pro.streaming.R5Connection;
import com.red5pro.streaming.R5Stream;
import com.red5pro.streaming.R5StreamProtocol;
import com.red5pro.streaming.config.R5Configuration;
import com.red5pro.streaming.view.R5VideoView;
import com.sneak8.sneak8.R;


public class SubscribeFragment extends Fragment {

    protected  R5Stream stream;
    protected static boolean isSubscribing = false;

    public SubscribeFragment() {
        // Required empty public constructor
    }


    public static SubscribeFragment newInstance() {
        SubscribeFragment fragment = new SubscribeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public R5Configuration configuration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources res = getResources();
        configuration = new R5Configuration(R5StreamProtocol.RTSP,res.getString(R.string.domain),  8554, "live", 1.0f);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_subscribe, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Button publishButton = (Button) getActivity().findViewById(R.id.subscribeButton);
        publishButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                onSubscribeToggle();
            }
        });

    }

    // Toggle Subscribe button
    private void onSubscribeToggle() {
        Button subscribeButton = (Button) getActivity().findViewById(R.id.subscribeButton);
        if(isSubscribing) {
            stop();
        }
        else {
            start();
        }
        isSubscribing = !isSubscribing;
        subscribeButton.setText(isSubscribing ? "stop" : "startLiveStream");
    }

    public void start(){
        R5VideoView videoView = (R5VideoView) getActivity().findViewById(R.id.subscribeView);

        stream = new R5Stream(new R5Connection(configuration));
        videoView.attachStream(stream);
        stream.play("red5prostream");       //TODO get stream name and play here

    }

    public void stop(){
        if(stream != null){
            stream.stop();
        }
    }

    // State management to manage moving between tabs
    @Override
    public void onPause() {
        super.onPause();
        if(isSubscribing) {
            onSubscribeToggle();
        }
    }
}
