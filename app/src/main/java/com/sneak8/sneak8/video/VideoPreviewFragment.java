package com.sneak8.sneak8.video;

import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.VideoView;

import com.sneak8.sneak8.R;

import java.io.IOException;

/**
 * Created by adrian on 4/9/17.
 */
public class VideoPreviewFragment extends Fragment implements OnClickListener, SurfaceHolder.Callback {
    MediaRecorder recorder;
    SurfaceHolder holder;
    boolean recording = false;
    ImageButton publishButton;      // record/stop button



    // Returns an instance of the VideoCaptureFragment to be used for recording videos
    public static VideoPreviewFragment newInstance() {
        VideoPreviewFragment fragment = new VideoPreviewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VideoView vv;

        vv = (VideoView) getActivity().findViewById(R.id.videoPreview);


        // Video loop using MediaPlayer
        vv.setOnPreparedListener (new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
            // TODO add android default file path and video
        //    Uri uri = Uri.parse("android.resource://com.example/"
          //          + R.raw.video);

            //vv.setVideoURI(uri);
            vv.requestFocus();
            vv.start();
        }




    // Return our publish Fragment view when this fragments view is called
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_videopreview,container,false);
        return v;
    }


    private void initRecorder() {
        recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

        CamcorderProfile cpHigh = CamcorderProfile
                .get(CamcorderProfile.QUALITY_HIGH);
        recorder.setProfile(cpHigh);
        recorder.setOutputFile("/sdcard/videocapture_example.mp4");
        recorder.setMaxDuration(10000); // 10 seconds
        recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
    }

    private void prepareRecorder() {
        recorder.setPreviewDisplay(holder.getSurface());

        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            getActivity().finish();
        } catch (IOException e) {
            e.printStackTrace();
            getActivity().finish();
        }
    }

    public void onClick(View v) {
        if (recording) {
            recorder.stop();
            recording = false;

            // Let's initRecorder so we can record again
            initRecorder();
            prepareRecorder();
        } else {
            recording = true;
            recorder.start();
        }
        publishButton.setImageResource(recording ? R.drawable.ic_recordbutton_after:R.drawable.ic_recordbutton_before);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        prepareRecorder();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (recording) {
            recorder.stop();
            recording = false;
        }
        recorder.release();
        getActivity().finish();
    }
}