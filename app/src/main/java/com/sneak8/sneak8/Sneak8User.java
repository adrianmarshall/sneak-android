package com.sneak8.sneak8;

import android.media.Image;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by adrian on 12/15/15.
 */

// Common functions to interact with Sneak8 Users and Sneak8 event API

public class Sneak8User {

    static String adminUsername = "adrian";
    static String adminPassword = "denver10";

    // Sneak8 User attributes
    private String user_id;
    private String first_name;
    private String last_name;
    private String username;
    private String email;
    private String type;
    private Image profile_image;        //TODO change to type that can be uploaded to server


    //Sneak8 empty Constructor
    public Sneak8User(){

    }

    /**
     * This constructor is used to Create a user in the backend
     * When the password param is not given a new user will not be created in the Sneak8 system but just an object created on this platform
     *
     * @param first_name
     * @param last_name
     * @param username
     * @param email
     * @param type
     * @param password
     */
    public Sneak8User(String first_name,String last_name,String username,String email,String type,String password){
        try {
            JSONObject theUser = createUser(first_name,last_name,username,email,type,password);
            this.first_name = theUser.getString("first_name");
            this.last_name = theUser.getString("last_name");
            this.username = theUser.getString("username");
            this.email = theUser.getString("email");
            this.type = theUser.getString("type");
            // TODO Set users username and password to save to device

        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }

    }

    /**
     * This constructor is used to Create a user in the backend
     * When the password param is not given a new user will not be created in the Sneak8 system but just an object created on this platform
     *
     * @param first_name
     * @param last_name
     * @param username
     * @param email
     * @param type
     */
    public Sneak8User(String first_name,String last_name,String username,String email,String type){

            this.first_name = first_name;
            this.last_name = last_name;
            this.username = username;
            this.email = email;
            this.type = type;


    }

    // Getters
    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public Image getProfile_image() {
        return profile_image;
    }

    public static JSONObject createUser(String first_name,String last_name,String username,String email,String type,String password){

       //TODO get usenarme and password, remove hardcoded username and password


        JSONObject theUser = null;
        JSONObject parameters = new JSONObject();
        String createUserApiURL = "/users/~api/v1/";

        try{
            parameters.put("first_name",first_name);
            parameters.put("last_name",last_name);
            parameters.put("username",username);
            parameters.put("email",email);
            parameters.put("type",type);
            parameters.put("password",password);
        }catch (JSONException e){
            e.getMessage();
            e.printStackTrace();
        }



        // query the API for the event given the event id. the event is returned in a JSON Object format

        try {

            String allData = HttpObject.httpMakeRequest("POST",createUserApiURL,parameters);        // Returns all user datea in JSOn format
            theUser = new JSONObject(allData);
            Log.d("New User created: ", theUser.toString() );


            if(theUser != null) {
                System.out.println("User with id " + theUser.getString("id") + " created: " + theUser);
            }else
            {
                System.out.println("Could not create user");
            }

           // httpCon.disconnect();
        } catch(Exception e){
            Log.d("Err:createUser"," See below for stack trace");
           e.printStackTrace();
        }


        return theUser;        // Returns the JSON user requested
    }


    /**
     * This function takes a JSON Array list of Users and converts them to an Arraylist
     * note: The API gets users and returns them in a "results" JSONArray
     * @param jsonArray
     * @return ArrayList<Sneak8User>
     */
    private static ArrayList<Sneak8User> jsonArrayToSneak8ArrayList(JSONArray jsonArray){

        ArrayList<Sneak8User> sneak8ArrayList = new ArrayList<Sneak8User>();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonUser = jsonArray.getJSONObject(i);
                String userID = jsonUser.getString("id");
                String firstName = jsonUser.getString("first_name");
                String lastName = jsonUser.getString("last_name");
                String userName = jsonUser.getString("username");
                String email = jsonUser.getString("email");
                String type = jsonUser.getString("type");

                Sneak8User tempUser = new Sneak8User(firstName,lastName,userName,email,type);

                sneak8ArrayList.add(tempUser);

                Log.d("JsonArray to list: ", " Successful in translating JSONArray to Sneak8User ArrayList");
            }
        }catch (JSONException e){
            Log.d("jsonArray to list: ", " Error translating JSONArray to Sneak8User Arraylist");

            e.getMessage();
            e.printStackTrace();
        }


        return sneak8ArrayList;

    }

    @Override
    public String toString(){

        return "User's username: " + this.username;
    }


}