package com.sneak8.sneak8;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.sneak8.utility.AWSHelper;
import com.sneak8.utility.DateFormater;
import com.sneak8.utility.video.videoUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import static com.sneak8.sneak8.TrendingActivity.TAG_EVENT_IMAGE;
import static com.sneak8.sneak8.TrendingActivity.TAG_ID;
import static com.sneak8.utility.video.videoUtil.addVideoToList;
import static com.sneak8.utility.video.videoUtil.eventVideos;
import static com.sneak8.utility.video.videoUtil.getVideosTask;

/**
 * Created by adrian on 11/3/15.
 */
public class EventDetailActivity extends Activity implements  OnClickListener{

    JSONObject theEvent = null;         // JSONObject that will hold the event information
    JSONArray eventComments = null;     // JSONArray that will hold all of the comments/chats for the event
    ArrayList<String> commentsList = new ArrayList<String>();       // Final String comment that will go in Chat/Comment table
    ImageView eventImage;
    ImageView btnMap;
    TextView txtDescription;
    TextView txtDateTime;
    TextView txtDay;
    TableLayout chatTable;
    ImageView btnHome;
    ImageView btnSneakPeak;
    ImageView btnProfile;
    Button btnSendComment;
    EditText txtEnterChat;
    String txtCommentContent = "null--error if can see this";
    View navBar;

    // Buttons
    ImageButton leftArrowVideoSwitcher, rightArrowVideoSwitcher;

    ViewSwitcher simpleViewSwitcher;
    VideoView videoView;


    String event_id;            // id for the event. We'll get this from the activity that called it
    ProgressBar spinner;        // progress bar to show while loading event


    DateFormater dateFormater = new DateFormater();

    public static AWSHelper awsHelper;
    // Amazon TransferObserver for transfering files
    TransferObserver observer;
    TransferObserver videoDownloadObserver;
    List<TransferObserver> videoObservers;

    int currentVideoIndex = 0;              // TODO delete if not using- index will always start with the first video. Question to self: Will this always be the case??
    ListIterator<File> videoIterator ;   // List iterator will always start with the first video. Question to self: Will this always be the case??


    public enum Task{SUBMIT_COMMENT,GET_EVENT}

    Task executeTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventdetail2);

       // ImageView tempBackground = (ImageView) findViewById(R.id.imgEventDetailTemp);         this was for testing purposes/mock screen
        spinner = (ProgressBar)findViewById(R.id.detailProgressBar);     // connect progress bar

        // Initialize AWS Helper object to initialize AWS Credentials
        awsHelper  = new AWSHelper(getApplicationContext() );

        /*
        tempBackground.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Intent i = new Intent(EventDetailActivity.this, _______.class);
                //startActivity(i);
                finish();
            }

        });

        */

        Bundle extras = null;
        event_id = null;		// The id of the event given from the "TrendingActivity"

        // Getting The event Id from when it was clicked on in the TrendingActivity activity
        if(savedInstanceState == null){
            extras = getIntent().getExtras();
            if(extras == null) {

                event_id = null;
                Log.d("Event ID NULL: ", "Failed to get eventID");
                
            }
            else{
                event_id = extras.getString("event_id");
                Log.d("Event ID: ", event_id);
            }


        } else{
            event_id = (String) savedInstanceState.getSerializable("event_id");
        }

        // connect UI components
        txtDescription = (TextView) findViewById(R.id.detail_description);
        eventImage = (ImageView) findViewById(R.id.detailEventImage);
        videoView = (VideoView) findViewById(R.id.detailEventVideo);
        btnMap = (ImageView) findViewById(R.id.detail_imgMap);
        txtDateTime = (TextView) findViewById(R.id.detail_dateTime);
        chatTable = (TableLayout) findViewById(R.id.tblChat);
        navBar = findViewById(R.id.detail_navBar);
        btnHome = (ImageView) navBar.findViewById(R.id.btnHome);
        btnSneakPeak = (ImageView) navBar.findViewById(R.id.btnSneakPeak);
        btnProfile = (ImageView) navBar.findViewById(R.id.btnProfile);
        txtDay = (TextView) findViewById(R.id.detail_eventDay);
        btnSendComment = (Button) findViewById(R.id.detail_btnsendComment);
        txtEnterChat = (EditText) findViewById(R.id.txtEnterChat);

        leftArrowVideoSwitcher = (ImageButton) findViewById(R.id.btn_left_arrow_switcher);
        rightArrowVideoSwitcher = (ImageButton) findViewById(R.id.btn_right_arrow_switcher);

        simpleViewSwitcher = (ViewSwitcher) findViewById(R.id.simpleViewSwitcher); // get the reference of ViewSwitcher


        btnMap.setOnClickListener(this);
        btnHome.setOnClickListener(this);
        btnSneakPeak.setOnClickListener(this);
        btnProfile.setOnClickListener(this);
        btnSendComment.setOnClickListener(this);
        leftArrowVideoSwitcher.setOnClickListener(this);
        rightArrowVideoSwitcher.setOnClickListener(this);

        // Toggle Arrow video navigation buttons
        // This should hide the buttons initially since videos aren't loaded yet.
        toggleVideoButtonSwitcherView();


        new getEvent().execute(Task.GET_EVENT);       // get the event from server

    }

    // Listners for when objects are clicked.
    @Override
    public void onClick(View v) {

        if (v == btnMap) {
            if(theEvent == null){
                Toast.makeText(getApplicationContext(),"Event Address not loaded yet",Toast.LENGTH_SHORT).show();
            }
            else{
                // TODO Get the business's information from API
                //TODO: Copy the event's address to user clipboard
                Toast.makeText(getApplicationContext()," 16th Peachtree st, Atlanta,GA 30340",Toast.LENGTH_SHORT).show();
                //TODO delete one of these Intents. It's redundant
                Intent businessPageIntent = new Intent(EventDetailActivity.this,BusinessPageActivity.class);


                try {
                    String business_id = theEvent.getString("business");
                    businessPageIntent.putExtra("business_id", business_id);        //sends the event id to  EventDetailActivity

                    startActivity(businessPageIntent);
                }catch (Exception e){
                    e.getMessage();
                    e.printStackTrace();
            }
        }
        }
        // Navigate to home page
        if( v == btnHome){
            Intent home = new Intent(EventDetailActivity.this,TrendingActivity.class);
            startActivity(home);
        }

        // Navigate to Trending page
        if(v == btnSneakPeak){
            Intent home = new Intent(EventDetailActivity.this,SneakPeakActivity.class);
            startActivity(home);
        }

        // Navigate to Profile page     // TODO create User Profile activity and navigate to it
        if(v == btnProfile){
           // Intent profile = new Intent(EventDetailActivity.this,UserProfileActivity.class);
           // startActivity(profile);
        }
        if(v == btnSendComment){
            //sends the comment to the server
            txtCommentContent = txtEnterChat.getText().toString();
            txtEnterChat.getText().clear();
            new getEvent().execute(Task.SUBMIT_COMMENT);
        }

        // If the user clicks on event Image show list of videos related to event
        if(v == eventImage){

            // Get all videos for this event id
          //  List<File> eventVideos = videoUtil.getEventVideos(event_id);
            // addUserVideosToMainView(eventVideos);

        //    new getEventVideos().execute();     // TODO - delete if not needed and function below working

            String[] params = new String[]{event_id};

            AsyncTask videosTask = new getVideosTask(new getVideosTask.AsyncResponse() {
                @Override
                public void processFinish( ArrayList<String> videoURIs) {
                    Log.d(" In processFinish ","Attempting to show videos in main view.");

                    // TODO download videos from S3
                    //  get videos from Amazon s3
                    // Initialize AWS Helper object to initialize AWS Credentials

                    // A List of all transfers


                     videoObservers = new ArrayList<TransferObserver>();

                    for(String videoURI : videoURIs) {
                        videoDownloadObserver = awsHelper.downloadS3File(videoURI);
                        Log.d("videoURI to download: ",  videoURI + ", File Path: " + videoDownloadObserver.getAbsoluteFilePath());

                        videoDownloadObserver.setTransferListener(new VideoDownloadListener());
                        videoObservers.add(videoDownloadObserver);
                    }

                   /*
                    try {

                        if (!eventVideos.isEmpty()) {
                       //     addUserVideosToMainView(eventVideos);   // TODO implement on UI thread (another asyncTask)
//          show event title in toast message
                            Log.d("updating videos for ", theEvent.getString(TrendingActivity.TAG_TITLE));
                            Log.d("Videos to add: ",eventVideos.toString());
                            Toast.makeText(EventDetailActivity.this, "Updating videos...", Toast.LENGTH_LONG).show();


                            // TODO update UI with videos


                        } else {
                            Toast.makeText(EventDetailActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                        }

                    }catch (JSONException e){
                        Log.d("Error:", e.getMessage());
                    }
                    */

                }
            }).execute(params);         // Execute task and pass parameters



        }

        if(v == rightArrowVideoSwitcher){
            // If there's a next video, show it
            if(videoIterator.hasNext()){
                switchVideo(videoIterator.next());
            }
        }

        if(v == leftArrowVideoSwitcher){

            // If there's a previous video, show it
            if(videoIterator.hasPrevious()){
                switchVideo(videoIterator.previous());
            }        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
    }
    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }
    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
    }
    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }



    public class getEvent extends AsyncTask<Task, Void, Boolean> {


        @Override
        protected void onPreExecute(){
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Task... params) {
            // TODO: attempt authentication against a network service.
            if(params[0] == Task.SUBMIT_COMMENT){


                //TODO get userid that should be saved on device with username. use userId '1' for testing
                String user_id = "1";

                boolean commentSubmitted = false;
                commentSubmitted = Sneak8Event.createEventComment(user_id,event_id,txtCommentContent);
                if(commentSubmitted == true){
                    Toast.makeText(getApplicationContext(),"Comment submitted",Toast.LENGTH_SHORT);
                }else {
                    Toast.makeText(getApplicationContext(),"Error Submitting comment",Toast.LENGTH_SHORT);
                }
                return  commentSubmitted;
            }else
                if(params[0] == Task.GET_EVENT) {
                try {
                    theEvent = new Sneak8Event().getSingleEvent(event_id);


                } catch (Exception e) {
                    Log.d("error: EventDetail", e.getMessage());
                }

                try {
                    eventComments = new Sneak8Event().getCommentsForEvent(event_id);        // Gets JSONArray of comments

                    loadComments();     // load comments from event
                /* Get comments
                JSONObject comment = eventComments.getJSONObject(0);
                String user = comment.getString("user");        // get the user of the comment
                String username = Sneak8Event.getCommentUsername(user);     // get the username for the comment

                System.out.println("Comment-> @" + username + comment.getString("content")); // test print the first comment
                Toast.makeText(getApplicationContext(),"@"+username+":"+comment.getString("content"),Toast.LENGTH_LONG).show();

                */
                } catch (Exception e) {
                    Log.d("Error getting comments:", e.getMessage());
                }


                if (theEvent == null)
                    return false;
                else
                    return true;
            }
            return  true;
        }

        // TODO: register the new account here.
        // return true;


        // @Override
        protected void onPostExecute(final Boolean success) {
            spinner.setVisibility(View.GONE);
            try {
                if (success) {
//          show event title in toast message
                    Log.d("event name: ", theEvent.getString(TrendingActivity.TAG_TITLE));
                    Toast.makeText(EventDetailActivity.this, theEvent.getString(TrendingActivity.TAG_TITLE), Toast.LENGTH_LONG).show();

                    mapEvent();         // maps all of the Events data to it's UI components

                    addCommentsToTable();           // Adds the comments to the table


                } else {
                    Toast.makeText(EventDetailActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                }

            }catch (JSONException e){
                Log.d("Error:", e.getMessage());
            }

        }

        // @Override
        protected void onCancelled() {
            Toast.makeText(EventDetailActivity.this, "Canceled finding out what's going on", Toast.LENGTH_SHORT);
        }
    }




    public class getEventVideos extends AsyncTask<Void, Void, List<File>> implements videoUtil.getVideosTask.AsyncResponse {

        ArrayList<String> videoURIs = null;

        @Override
        protected void onPreExecute(){
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<File> doInBackground(Void... params) {
            Log.d("GetEventVideos: ", " testing..");


            String[] args = new String[]{event_id};
                try {
                    // Get all videos for this event id
                    eventVideos = videoUtil.getEventVideos(event_id);
                    //execute the async task

                } catch (Exception e) {
                    Log.d("Error getting comments:", e.getMessage());
                }



            return  eventVideos;
        }

        // TODO: register the new account here.
        // return true;


         @Override
        protected void onPostExecute(final List<File> eventVideos) {



            spinner.setVisibility(View.GONE);
            try {
                if (!eventVideos.isEmpty()) {
                    addUserVideosToMainView();
//          show event title in toast message
                    Log.d("updating videos for ", theEvent.getString(TrendingActivity.TAG_TITLE));
                    Log.d("Videos to add: ",eventVideos.toString());
                    Toast.makeText(EventDetailActivity.this, "Updating videos...", Toast.LENGTH_LONG).show();

                  // TODO update UI with videos


                } else {
                    Toast.makeText(EventDetailActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                }

            }catch (JSONException e){
                Log.d("Error:", e.getMessage());
            }

        }

         @Override
        protected void onCancelled() {
            Toast.makeText(EventDetailActivity.this, "Canceled getting videos", Toast.LENGTH_SHORT);
        }

        @Override
        public void processFinish(ArrayList<String> videoURIs) {
            this.videoURIs = videoURIs;
        }


        // Override AsyncResponse's on Process finished to do something with returned data




    }
    // Map the data from the event to it's UI components
    public void mapEvent(){

        try {

            // connect UI components
            eventImage = (ImageView) findViewById(R.id.detailEventImage);


            observer = awsHelper.downloadS3File(theEvent.getString(TAG_EVENT_IMAGE));
            Log.d("fileNameToEvent: ", "Event ID: " + theEvent.get(TAG_ID) + ", File Path: " + observer.getAbsoluteFilePath());

            observer.setTransferListener(new MyDownloadListener());

            // set time for event
            String dateTimeofEvent = theEvent.getString(TrendingActivity.TAG_START_TIME);
            String tempDate = txtDateTime.getText().toString();
            dateTimeofEvent = tempDate + dateTimeofEvent;
            txtDay.setText(dateFormater.convertDateToDay(theEvent.getString(TrendingActivity.TAG_START_TIME)));
            txtDateTime.setText(dateFormater.convertDateToTime(theEvent.getString(TrendingActivity.TAG_START_TIME)));
           // txtDateTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

            txtDescription.setMovementMethod(new ScrollingMovementMethod());        // makes the text box scrollable
            txtDescription.setText(theEvent.getString(TrendingActivity.TAG_DESCRIPTION));           // sets the Description for the event

        }catch(Exception e){
            Log.d("Error:", e.getMessage());
        }
    }

    // Load Image/video
    public void loadMainImageView(){


        // TODO detect Mimey type
          // if video/not image type, change image view to video view
          // Load video into video view


        try {

            // The file where we store our image/video locally after download.
            File fileToLoadIntoView = new File(observer.getAbsoluteFilePath());

            // load image or video with Picasso image loader
            Picasso.with(this)
                    .load(fileToLoadIntoView)
                    .resize(600, 600)
                    .into(eventImage);

            eventImage.setOnClickListener(this);        // Set Listener to event image

        }catch (Exception e){
            System.out.println("error: "+ e.getMessage());
            e.printStackTrace();
        }

    }


    public void loadComments() {

        HashMap<String,String> usersOfComments = new HashMap<String,String>();     // This map will contain

        if(theEvent != null) {


            try {
                // Get comments

                // JSONObject comment = eventComments.getJSONObject(0);
                // String user = comment.getString("user");        // get the user of the comment
                // String username = Sneak8Event.getCommentUsername(user);     // get the username for the comment

                System.out.println(" Comments:  ");
                for (int i = 0; i < eventComments.length(); i++) {
                    JSONObject comment = eventComments.getJSONObject(i);
                    String user = comment.getString("user");
                    String username = Sneak8Event.getCommentUsername(user);     // get the username for the comment
                    String content = comment.getString("content");
                    String fullComment = "@" + username + ": " + content;
                    System.out.println(fullComment);

                    // Add comment to comment List
                    commentsList.add(fullComment);

                }
                // System.out.println("Comment-> @" + username + comment.getString("content")); // test print the first comment
                //Toast.makeText(getApplicationContext(), "@" + username + ":" + comment.getString("content"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                Log.d("err loading comments:", e.getMessage());
            }

        }else{
            Log.d("Event Error:", "Event is null,failed to get event before comments");
        }

    }

    public void addCommentsToTable(){

        System.out.println(" Comments List:  ");
        if(commentsList != null) {


            try {
                for (int i = 0; i < commentsList.size(); i++) {

                    TextView txtComment = new TextView(this);
                    txtComment.setText(commentsList.get(i));        // Sets the comment to a text view

                  //  txtComment.setLayoutParams(new ViewGroup.LayoutParams(
                    //        ViewGroup.LayoutParams.WRAP_CONTENT,
                      //      ViewGroup.LayoutParams.WRAP_CONTENT));
                    TableRow rowComment = new TableRow(this);

                    rowComment.setLayoutParams(new TableRow.LayoutParams(
                            TableRow.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT));



                    rowComment.addView(txtComment);     //adds the text to the row

                    chatTable.addView(rowComment);      // Adds the comment to the chat table

                    chatTable.setShrinkAllColumns(true);
                }
            }catch (Exception e){
                Log.d("Chat table err:", e.getMessage());
            }

        }else{
            Log.d("commentsList null: ", "There are no comments");
        }
    }


    // Add loaded videos to main view
    /*
        Add 1 video at a time to a list and then add to the Main view
     */
    public void addUserVideosToMainView(){



        // show the next view of ViewSwitcher
        simpleViewSwitcher.showNext();          // This should show the video view




        if(!eventVideos.isEmpty()) {


            try {
                // Request focus on first video and start it

                // The file where we store our image/video locally after download.
                File testVideoFile = eventVideos.get(0);     // TODO remove this and just be sure the first video plays




                videoView.setVideoPath(testVideoFile.getAbsolutePath());
                videoView.requestFocus();
                videoView.start();

                Toast.makeText(EventDetailActivity.this, "Showing first video", Toast.LENGTH_LONG).show();

                // Toggle Arrow video navigation buttons
                toggleVideoButtonSwitcherView();

            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
                e.printStackTrace();
            }

            Log.d("downloaded videos:",eventVideos.toString() );
        }
        // TODO make main view into scrollable video list




        // TODO add list of videos into scrollable video list

    }


    /*
       Show the next or previous video available
     */
    public void switchVideo(File videoToShow){

        // Toggle Arrow video navigation buttons
        toggleVideoButtonSwitcherView();

        // Stop current video playback
        videoView.stopPlayback();

        try {
            // Request focus on video and start it

            // The file where we store our image/video locally after download.
            // Get video to show

            videoView.setVideoPath(videoToShow.getAbsolutePath());
            videoView.requestFocus();
            videoView.start();



        } catch (Exception e) {
            Log.d("SwitchVideo error:", e.getMessage());
            e.printStackTrace();
        }

    }

    /*
       Toggle when the left and right ar row shows based on if there's a video avaialable in that direction
     */
    public void toggleVideoButtonSwitcherView(){

        if(videoIterator == null){
            return;
        }

        // If there are no previous videos to the left(i.e- first video)
        // don't show left arrow
        if(!videoIterator.hasPrevious()){
            leftArrowVideoSwitcher.setVisibility(View.INVISIBLE);
        }
        if(!videoIterator.hasNext()){
            rightArrowVideoSwitcher.setVisibility(View.INVISIBLE);
        }

        // Show arrow if a video exist to the left
        if(videoIterator.hasPrevious()){
            leftArrowVideoSwitcher.setVisibility(View.VISIBLE);
        }
        // Show arrow if a video exist to the left
        if(videoIterator.hasNext()){
            rightArrowVideoSwitcher.setVisibility(View.VISIBLE);
        }

        // Case no videos, don't show any arrows
        if(!videoIterator.hasNext() && !videoIterator.hasPrevious()){
            rightArrowVideoSwitcher.setVisibility(View.INVISIBLE);
            leftArrowVideoSwitcher.setVisibility(View.INVISIBLE);
        }
    }


    /* A TransferListener class that can listen to a download task and be
  * notified when the status changes.
  */
    private class MyDownloadListener implements TransferListener {
        // Simply updates the list when notified.

        String TAG = "Trending Activity: ";
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "onError: " + id, e);
            //   updateList();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            //  updateList();
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d(TAG, "onStateChanged: " + id + ", " + state);
            if (state.toString().equals("COMPLETED")) {
                Log.d(TAG, " Download complete. Adding image to screen for event id: " + id);
                loadMainImageView();
            }
        }
    }



    /* A TransferListener class that can listen to a download task and be
  * notified when the status changes.
  */
    private class VideoDownloadListener implements TransferListener {
        // Simply updates the list when notified.

        String TAG = "VideoDownloadListener: ";
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "onError: " + id, e);
            //   updateList();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged video: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            //  updateList();
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d(TAG, "onStateChanged: " + id + ", " + state);
            if (state.toString().equals("COMPLETED")) {
                Log.d(TAG, " Download complete. Adding video to screen for event id: " + id);
                addVideoToList(id);
                addUserVideosToMainView();          // Play the first video, add all others to list


                // If finished adding all videos, set Iterator
                if(eventVideos.size() == videoObservers.size()){
                    videoIterator = eventVideos.listIterator();
                }
            }
        }
    }
}
