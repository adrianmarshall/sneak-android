package com.sneak8.sneak8;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sneak8.sneak8.dao.Sneak8Business;
import com.sneak8.utility.DateFormater;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by adrian on 11/3/15.
 */
public class BusinessPageActivity extends Activity implements  OnClickListener {

    JSONObject theBusiness = null;         // JSONObject that will hold the event information
    TextView txt_name;
    TextView txt_Addressline1;
    TextView txt_Addressline2;


    String business_id;            // id for the event. We'll get this from the activity that called it
    ProgressBar spinner;        // progress bar to show while loading event


    DateFormater dateFormater = new DateFormater();


    public enum Task {SUBMIT_COMMENT, GET_BUSINESS}

    Task executeTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_businesspage);

        // ImageView tempBackground = (ImageView) findViewById(R.id.imgEventDetailTemp);         this was for testing purposes/mock screen
      //  spinner = (ProgressBar) findViewById(R.id.detailProgressBar);     // connect progress bar



        Bundle extras = null;
        business_id = null;        // The id of the event given from the "TrendingActivity"

        // Getting The event Id from when it was clicked on in the TrendingActivity activity
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {

                business_id = null;
                Log.d("Event ID NULL: ", "Failed to get eventID");

            } else {
                business_id = extras.getString("business_id");
                Log.d("Event ID: ", business_id);
            }


        } else {
            business_id = (String) savedInstanceState.getSerializable("business_id");
        }

        // connect UI components
        txt_name = (TextView) findViewById(R.id.txt_businessName);


        new getBusiness().execute(Task.GET_BUSINESS);       // get the event from server

    }

    @Override
    public void onClick(View v) {


    }


    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
    }

    @Override
    protected void onStop() {
        super.onStop();
        // The activity is no longer visible (it is now "stopped")
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
    }


    public class getBusiness extends AsyncTask<Task, Void, Boolean> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(Task... params) {
            // TODO: attempt authentication against a network service.
            if (params[0] == Task.SUBMIT_COMMENT) {


            } else if (params[0] == Task.GET_BUSINESS) {
                try {
                    theBusiness = new Sneak8Business().getSingleBusiness(business_id);


                } catch (Exception e) {
                    Log.d("error: BusinessPage", e.getMessage());
                }


                if (theBusiness == null)
                    return false;
                else
                    return true;
            }
            return true;
        }

        // TODO: register the new account here.
        // return true;


        // @Override
        protected void onPostExecute(final Boolean success) {

            try {
                if (success) {
//          show event title in toast message
                    Log.d("business name: ", theBusiness.getString("name"));
                    Toast.makeText(BusinessPageActivity.this, theBusiness.getString("name"), Toast.LENGTH_LONG).show();

                    txt_name.setText(theBusiness.getString("name"));

                } else {
                    Toast.makeText(BusinessPageActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                Log.d("Error:", e.getMessage());
            }

        }

        // @Override
        protected void onCancelled() {
            Toast.makeText(BusinessPageActivity.this, "Canceled finding out what's going on", Toast.LENGTH_SHORT);
        }
    }

    /*
    // Map the data from the event to it's UI components
    public void mapBusienss(){

        try {

            // connect UI components
            eventImage = (ImageView) findViewById(R.id.detailEventImage);

            // load image with Picasso image loader
            Picasso.with(this)
                    .load(theBusiness.getString(TrendingActivity.TAG_EVENT_IMAGE))
                    .resize(600, 600)
                    .into(eventImage);

            // set time for event

        }catch(Exception e){
            Log.d("Error:", e.getMessage());
        }
    }
    */

}

