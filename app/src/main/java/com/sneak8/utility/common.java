package com.sneak8.utility;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.red5pro.streaming.R5Connection;
import com.red5pro.streaming.R5Stream;
import com.red5pro.streaming.config.R5Configuration;
import com.red5pro.streaming.view.R5VideoView;
import com.sneak8.sneak8.HttpObject;
import com.sneak8.sneak8.R;
import com.sneak8.sneak8.video.PublishFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PublishFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PublishFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class common extends Activity{


    static SharedPreferences sharedPreferences;		// shared preferences will be used to store username & password information

    final String MyPREFERENCES = "MyPrefs" ;

    final static String APP_NAME = "live";
    private static String red5SecurityToken= "rkauhV8ZPz";
    private static String red5proHost = "localhost";

    Resources res; // Get Resources


    public common(Context context) {
        // Required empty public constructor
        sharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        red5proHost = res.getString(R.string.red5prohost);
        red5SecurityToken = res.getString(R.string.red5pro_security_token);

        res = getResources();


    }

    /**
     *  Returns an item from the shared preferences
     * @param key
     * @return
     */
    public static String getSharedPreferenceItem(String key){
        return sharedPreferences.getString(key,"");
    }

    /**
     *  stores a string object in the shared preferences
     * @param key
     * @return
     */
    public static void setSharedPreferenceItem(String key,String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(key, value);	// Adding the username to the preferences

        editor.commit();	// Commits (saves) the username and password data we just added

        if(sharedPreferences.contains(key) ){
            Log.d("StoreObject:",key + " has been stored");
        }else{
            Log.d("StoreObject:",key + " was NOT stored");
        }
    }
    public static String createPublishStreamID(String eventID){


        String usernameKey = "username";
        String username = "";
        String streamID = "";


        // If the user's username and password is in the shared preferences then we will log them in
        if(sharedPreferences.contains(usernameKey)){
            try{
                username = sharedPreferences.getString(usernameKey, "");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        /**
         * Format that the Event Stream will be created is "<EventID> -<Username>"
         * Note that the eventID and username are separated by a hyphen. this can be parsed out later
         */
        streamID = eventID +"-"+username;
        Log.d("StreamID:",streamID);

        return streamID;
    }


    public static String createSubscribeStreamID(String eventID){


        String usernameKey = "username";
        String username = "";
        String streamID = "";


        // If the user's username and password is in the shared preferences then we will log them in
        if(sharedPreferences.contains(usernameKey)){
            try{
                username = sharedPreferences.getString(usernameKey, "");
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        /**
         * Format that the Event Stream will be created is "<EventID> -<Username>"
         * Note that the eventID and username are separated by a hyphen. this can be parsed out later
         */
        streamID = eventID +"-"+username;
        Log.d("StreamID:",streamID);

        return streamID;
    }

    /**
     * Play,Stop, Toggle video streams
     */
    // Toggle Subscribe button
    private void onSubscribeToggle(R5Configuration configuration, R5Stream stream, R5VideoView videoView,String eventID,boolean isSubscribing,Boolean publishing) {
        /**Arguments:
         configuration = the confiugration settings for R5 to start streaming
         stream = the R5Stream in which the video is live streaming through
         videoView = The view in which the stream will be showing
         eventID = ID for the event that is streaming currently
         */
        if(isSubscribing) {
            stopLiveStream(stream);
        }
        else {
            startLiveStream( configuration,  stream,  videoView, eventID,publishing);
        }
        isSubscribing = !isSubscribing;
        //  subscribeButton.setText(isSubscribing ? "stop" : "startLiveStream");
    }

    public void startLiveStream(R5Configuration configuration, R5Stream stream, R5VideoView videoView,String eventID,Boolean publish){
        /*Arguments:
         configuration = the confiugration settings for R5 to start streaming
         stream = the R5Stream in which the video is live streaming through
         videoView = The view in which the stream will be showing
         eventID = ID for the event that is streaming currently
         */
        stream = new R5Stream(new R5Connection(configuration));
        videoView.attachStream(stream);
        String streamID;

        // if publish is true that means we are sending a stream so create publish id, else
        // we are subscribing to a stream and maybe create the id differently
        if(publish){
            streamID = createPublishStreamID(eventID);
        }else{
            streamID = createSubscribeStreamID(eventID);
        }
        stream.play(streamID);

    }

    public static void stopLiveStream(R5Stream stream) {
        /*Arguments:
         stream = the R5Stream in which the video is live streaming through
         */
        if (stream != null) {
            stream.stop();
        }
    }

    public static String getAllLiveStreams(){
        /*
         returns a list of all the live streams available
         URI format for endpoint: http://{host}:5080/api/v1/applications/{appname}/streams?accessToken={security-token}
         */

        String liveStreams = "";

        String getStreamsEndpoint = "http://"+red5proHost+":5080/api/v1/applications/"+APP_NAME+"/streams?accessToken="+red5SecurityToken;
        liveStreams = HttpObject.httpMakeRequest("GET",getStreamsEndpoint,null);

        if(liveStreams == null){
            liveStreams = "";
        }
        Log.d("GetAllStreamsOutput:",liveStreams);


        return liveStreams;

    }



    /** Detects the MimeType of a file and returns the appropiate mimetype
    @param filePath - The Path of the file we are detecting the mimetype for.

     @return type - The type of file that was passed in. Returns null if no extension was found on the file.

     */
    public static String detectMimeType(String filePath){


        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
