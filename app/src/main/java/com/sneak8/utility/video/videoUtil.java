package com.sneak8.utility.video;

import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.sneak8.sneak8.EventDetailActivity;
import com.sneak8.sneak8.HttpObject;
import com.sneak8.utility.AWSHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adrian on 6/17/17.
 */

public class videoUtil {

    HttpObject httpObject;
    static JSONArray apiResults = new JSONArray();

    public static List<File> eventVideos = new ArrayList<File>();

    static AWSHelper awsHelper = EventDetailActivity.awsHelper;        // TODO need a common place to hold this object outside of EventDetailActivity
    // Amazon TransferObserver for transfering files
    static TransferObserver observer;

    public static final String VIDEO_EVENT_TAG = "video_file";

    public static List<File> getEventVideos(String eventId){

        eventVideos = null;
        String apiEndpoint = "/videos/~api/v1/?format=json&eventId="+eventId;
        String httpMethod = "GET";

        AsyncTask getVideos = new getVideosTask(new getVideosTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<String> videoURIs) {
                // TODO add code here if using function. if not, delete getEventVidoes
            }
        });
        String[] params = new String[]{apiEndpoint,httpMethod};



        getVideos.execute(params);


        Log.d("video: ", "finished loading! ");



        return eventVideos;


    }
    /* A TransferListener class that can listen to a download task and be
    * notified when the status changes.
    */
    private static class MyDownloadListener implements TransferListener {
        // Simply updates the list when notified.

        String TAG = "VideoUtil: ";
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "onError: " + id, e);
            //   updateList();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged video: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            //  updateList();
        }

        @Override
        public void onStateChanged(int id, TransferState state) {
            Log.d(TAG, "onStateChanged: " + id + ", " + state);
            if (state.toString().equals("COMPLETED")) {
                Log.d(TAG, " Download complete. Adding video to screen for event id: " + id);
                addVideoToList(id);
            }
        }
    }

    // Add video file to List of videos
    public static void addVideoToList(int transferObserverId){

        awsHelper = EventDetailActivity.awsHelper;      // To Avoid Null Pointer Exception get the awsHelper again AFTER initialized in EventDetail

        // Get the Transfer Observer for the finished TransferObserver file
        TransferObserver myObserver = awsHelper.getTransferUtility().getTransferById(transferObserverId);
        // The file where we store our image/video locally after download.
        File videoFile = new File(myObserver.getAbsoluteFilePath());

        eventVideos.add(videoFile);

    }


    /*
     String  - Parameter type
     Void - Progress type
     JSONArray - Return type - Will return arrays of video objects if any
     */
    public static class getVideosTask extends AsyncTask<String, Void, JSONArray> {

        /*
        @param params - String parameters that are passed into execute method

        @param[0] - eventId

         */

        String apiEndpoint = "/videos/~api/v1/?format=json&eventId=";
        String httpMethod = "GET";

        String[] params = new String[]{apiEndpoint,httpMethod};





        public interface AsyncResponse {
            void processFinish(ArrayList<String> videoURIs);
        }

        public AsyncResponse delegate = null;

        public getVideosTask(AsyncResponse delegate){
            this.delegate = delegate;
        }

        @Override
        protected JSONArray doInBackground(String... params) {

            String eventId = params[0];     // Get eventId passed in as first parameter

            apiEndpoint += eventId;

            JSONArray results = null;

            try {

                String allData = HttpObject.httpMakeRequest(httpMethod,apiEndpoint,null);        // null for 3rd param is for JSON parameters for POST request
                JSONObject allDataJSON = new JSONObject(allData);
                Log.d("video count: ", allDataJSON.getString("count"));
                apiResults = allDataJSON.getJSONArray("results");    // Gets all of the results/ events

            } catch(Exception e){
                Log.d("Err:getVideosTask"," See below for stack trace");
                e.printStackTrace();
            }
            // TODO issue- doInBackground not returning to onPostExecute 
            return apiResults;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            Log.d("post execute,videos: "," post execute");
            apiResults = result;        // set results to global apiResults variable


            Log.d("video: ", "finished loading! ");

            // Videos are done loading by this point. Get video Files
            ArrayList<String> videoURIs = new ArrayList<>();

            try {
                // Add all video URI's to a list to prepare for download
                for (int i = 0; i < apiResults.length(); i++) {
                    videoURIs.add(apiResults.getJSONObject(i).getString(VIDEO_EVENT_TAG));
                    Log.d("video util: " , " added uri "+ apiResults.getJSONObject(i).getString(VIDEO_EVENT_TAG));
                }

            }catch (Exception e){
                Log.d("Error getting videos: ",e.getMessage());
                e.printStackTrace();
            }

            //  get videos from Amazon s3
            // Initialize AWS Helper object to initialize AWS Credentials

            // A List of all transfers

            /*


            List<TransferObserver> observers = new ArrayList<TransferObserver>();

            for(String videoURI : videoURIs) {
                observer = awsHelper.downloadS3File(videoURI);
                Log.d("videoURI to download: ",  videoURI + ", File Path: " + observer.getAbsoluteFilePath());

                observer.setTransferListener(new MyDownloadListener());
                observers.add(observer);
                break;  //TODO  remove later - This makes it so only one file is downloaded.
            }

            boolean finishedDownload = false;

            int downloadsComplete = 0;

            // wait for all downloads to complete before returning list of videos
            while(downloadsComplete <= observers.size()){

                // TODO possible error if adding to downloadsComplete the same download that we've previously passed and added as completed
                // TODO make list of completed observers. Add each completed to list,finished when completed list == original list
                for(TransferObserver theObserver: observers){
                    if(theObserver.getState() == TransferState.COMPLETED) {
                        downloadsComplete++;
                    }
                }
            }
        */
            // return eventVideos;
            delegate.processFinish(videoURIs);


        }
    }

}