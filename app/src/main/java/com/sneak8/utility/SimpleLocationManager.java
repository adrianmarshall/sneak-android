package com.sneak8.utility;

/**
 * Created by adrian on 1/26/17.
 */


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.Geofence;
import com.sneak8.sneak8.R;

import java.util.ArrayList;
import java.util.List;

import io.nlopez.smartlocation.OnActivityUpdatedListener;
import io.nlopez.smartlocation.OnGeofencingTransitionListener;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.geofencing.model.GeofenceModel;
import io.nlopez.smartlocation.geofencing.utils.TransitionGeofence;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;

public class SimpleLocationManager extends Activity implements OnLocationUpdatedListener, OnActivityUpdatedListener, OnGeofencingTransitionListener {

    private TextView locationText;
    private TextView activityText;
    private TextView geofenceText;

    Context mContext;

    private LocationGooglePlayServicesProvider provider;

    private static final int LOCATION_PERMISSION_ID = 1001;

    public SimpleLocationManager(Context context){
            mContext = context;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_ID && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocation();
        }
    }

    private void showLast() {
        Location lastLocation = SmartLocation.with(this).location().getLastLocation();
        if (lastLocation != null) {
            locationText.setText(
                    String.format("[From Cache] Latitude %.6f, Longitude %.6f",
                            lastLocation.getLatitude(),
                            lastLocation.getLongitude())
            );
        }

        DetectedActivity detectedActivity = SmartLocation.with(this).activity().getLastActivity();
        if (detectedActivity != null) {
            activityText.setText(
                    String.format("[From Cache] Activity %s with %d%% confidence",
                            getNameFromType(detectedActivity),
                            detectedActivity.getConfidence())
            );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (provider != null) {
            provider.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void startLocation() {

        provider = new LocationGooglePlayServicesProvider();
        provider.setCheckLocationSettings(true);

        SmartLocation smartLocation = new SmartLocation.Builder(mContext).logging(true).build();

        smartLocation.location(provider).start(this);
        smartLocation.activity().start(this);

        Log.d("Location: ","Location started!");

        // Create some geofences
      //  GeofenceModel mestalla = new GeofenceModel.Builder("1").setTransition(Geofence.GEOFENCE_TRANSITION_ENTER).setLatitude(39.47453120000001).setLongitude(-0.358065799999963).setRadius(500).build();
      //  smartLocation.geofencing().add(mestalla).start(this);
    }

    public void stopLocation() {
        SmartLocation.with(mContext).location().stop();
        Log.d("Location: ","Location stopped!");

        SmartLocation.with(this).activity().stop();
        Log.d("Location: ","Activity Recognition stopped!");

      //  SmartLocation.with(this).geofencing().stop();
        // geofenceText.setText("Geofencing stopped!");
    }
    // need to return the user's geo-location and try to match location with one in sneak8 database
    public void showLocation(Location location) {
        if (location != null) {
            final String text = String.format("Latitude %.6f, Longitude %.6f",
                    location.getLatitude(),
                    location.getLongitude());
          //  locationText.setText(text);
            Toast.makeText(mContext,text,Toast.LENGTH_LONG);
            Log.d(" Lat & Long", text);

            // We are going to get the address for the current position
            SmartLocation.with(mContext).geocoding().reverse(location, new OnReverseGeocodingListener() {
                @Override
                public void onAddressResolved(Location original, List<Address> results) {
                    if (results.size() > 0) {
                        Address result = results.get(0);
                        StringBuilder builder = new StringBuilder(text);
                        builder.append("\n[Reverse Geocoding] ");
                        List<String> addressElements = new ArrayList<>();
                        for (int i = 0; i <= result.getMaxAddressLineIndex(); i++) {
                            addressElements.add(result.getAddressLine(i));
                        }
                        builder.append(TextUtils.join(", ", addressElements));
                      //  locationText.setText(builder.toString());
                        Toast.makeText(mContext,builder.toString(),Toast.LENGTH_LONG);
                        Log.d("Location Address: ",builder.toString());
                    }
                }
            });
        } else {
            Toast.makeText(mContext,"Null location",Toast.LENGTH_LONG);
        }
    }

    /*
    private void showActivity(DetectedActivity detectedActivity) {
        if (detectedActivity != null) {
            Toast.makeText(getApplicationContext(),String.format("Activity %s with %d%% confidence",
                    getNameFromType(detectedActivity),
                    detectedActivity.getConfidence())
                    ,Toast.LENGTH_LONG);

        } else {
            Toast.makeText(getApplicationContext(),"Null Activity",Toast.LENGTH_LONG);
        }
    }
    */

    private void showGeofence(Geofence geofence, int transitionType) {
        if (geofence != null) {
            geofenceText.setText("Transition " + getTransitionNameFromType(transitionType) + " for Geofence with id = " + geofence.getRequestId());
        } else {
            geofenceText.setText("Null geofence");
        }
    }

    public Location getLocation(){
        return SmartLocation.with(mContext).location().getLastLocation();
    }

    @Override

    public void onLocationUpdated(Location location) {
       // showLocation(location);
    }




    @Override
    public void onActivityUpdated(DetectedActivity detectedActivity) {
     //   showActivity(detectedActivity);
    }


    @Override
    public void onGeofenceTransition(TransitionGeofence geofence) {
        showGeofence(geofence.getGeofenceModel().toGeofence(), geofence.getTransitionType());
    }

    private String getNameFromType(DetectedActivity activityType) {
        switch (activityType.getType()) {
            case DetectedActivity.IN_VEHICLE:
                return "in_vehicle";
            case DetectedActivity.ON_BICYCLE:
                return "on_bicycle";
            case DetectedActivity.ON_FOOT:
                return "on_foot";
            case DetectedActivity.STILL:
                return "still";
            case DetectedActivity.TILTING:
                return "tilting";
            default:
                return "unknown";
        }
    }

    private String getTransitionNameFromType(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "enter";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "exit";
            default:
                return "dwell";
        }
    }
}