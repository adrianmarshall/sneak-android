package com.sneak8.utility;

import android.content.Context;
import android.os.Environment;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by adrian on 4/23/17.
 */

public class AWSHelper {



    public static final String MY_BUCKET = "eagle.eye";
    //  public static final String OBJECT_KEY = "AKIAJRYYSXACQMLZTQ4Q";     //TODO may need to delete this when figured out object key



    public Context appContext;
    CognitoCachingCredentialsProvider credentialsProvider;
    CognitoSyncManager syncClient;

    // Amazon S3 Client
    AmazonS3 s3;



    TransferUtility transferUtility;

    public AWSHelper(){

    }

    public AWSHelper(Context applicationContex){
        this.appContext = applicationContex;            // Passing in the Application Context from the calling activity

        // Initialize the Amazon Cognito credentials provider
         this.credentialsProvider = new CognitoCachingCredentialsProvider(
                appContext,
                "us-east-1:9ec69bc0-3f2c-4582-acc0-749b5e57348a",   // Identity Pool ID
                Regions.US_EAST_1 // Region
        );


    /*
        // initialize a credentials provider object with your Activity’s context and
    // the values from your identity pool
        this.credentialsProvider = new CognitoCachingCredentialsProvider(
                appContext, // get the context for the current activity
                "172663388263", // your AWS Account id
                "us-east-1:9ec69bc0-3f2c-4582-acc0-749b5e57348a", // your identity pool id
                "arn:aws:iam::172663388263:role/Cognito_pulseAuth_Role",// an authenticated role ARN
                "arn:aws:iam::172663388263:role/Cognito_pulseUnauth_Role", // an unauthenticated role ARN
                Regions.US_EAST_1 //Region
        );

    */

        // Initialize the Cognito Sync client
         this.syncClient = new CognitoSyncManager(
                appContext,
                Regions.US_EAST_1, // Region
                credentialsProvider);

        // Create an S3 client
        this.s3 = new AmazonS3Client(credentialsProvider);
        // Set the region of your S3 bucket
        this.s3.setRegion(Region.getRegion(Regions.US_EAST_1));

        this.transferUtility = new TransferUtility(s3, appContext);


    }


    /**
     *
     * @param OBJECT_KEY - The File path for the s3 file I want to download( image or video)
     * @return observer - Observer object to keep track of state of download and progress as well as get the file path for downloaded file.
     */
    public TransferObserver downloadS3File(String OBJECT_KEY){

        // Location to download files from S3 to. You can choose any accessible
        // file.
        File MY_FILE = new File(Environment.getExternalStorageDirectory().toString() + "/" + "test"+OBJECT_KEY.length());

        // URI example:  https://eagle.eye.s3.amazonaws.com/media/photos/events/1/havana-club-atlanta-05.jpg
        // Just need everything after '.com/'

        String pattern = "(?<=.com/).*";        // REGEX to match everything after '.com/'

        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(OBJECT_KEY);
        if (m.find( )) {
            OBJECT_KEY = m.group(0);
        }




        //OBJECT_KEY = "media/photos/events/3/GoldRoom.jpg";      // TODO delete this line

        TransferObserver observer = transferUtility.download(
                MY_BUCKET,     // The bucket to download from
                OBJECT_KEY,    // The key for the object to download ( file path+file name in AWS )
                MY_FILE        // The file to download the object to
        );


        return observer;            // use the observer and get the absolute file path of the downloaded file
    }


    public void setApplicationContext(Context context){
        this.appContext = context.getApplicationContext();      // Get Application context to avoid Memory leak / NPE
    }

    public TransferUtility getTransferUtility() {
        return transferUtility;
    }

    /**
     * Converts number of bytes into proper scale.
     *
     * @param bytes number of bytes to be converted.
     * @return A string that represents the bytes in a proper scale.
     */
    public static String getBytesString(long bytes) {
        String[] quantifiers = new String[] {
                "KB", "MB", "GB", "TB"
        };
        double speedNum = bytes;
        for (int i = 0;; i++) {
            if (i >= quantifiers.length) {
                return "";
            }
            speedNum /= 1024;
            if (speedNum < 512) {
                return String.format("%.2f", speedNum) + " " + quantifiers[i];
            }
        }
    }

    /*
    * Fills in the map with information in the observer so that it can be used
    * with a SimpleAdapter to populate the UI
    */
    public static void fillMap(Map<String, Object> map, TransferObserver observer) {
        int progress = (int) ((double) observer.getBytesTransferred() * 100 / observer
                .getBytesTotal());
        map.put("id", observer.getId());
        map.put("fileName", observer.getAbsoluteFilePath());
        map.put("progress", progress);
        map.put("bytes",
                getBytesString(observer.getBytesTransferred()) + "/"
                        + getBytesString(observer.getBytesTotal()));
        map.put("state", observer.getState());
        map.put("percentage", progress + "%");
    }

}
